<?php
// +----------------------------------------------------------------------
// | 云静资源
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2022 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | Author: Uncle-L <1732983738@qq.com>
// +----------------------------------------------------------------------
// | 公共函数库
// +----------------------------------------------------------------------

/****************************************************** 数组 ******************************************************/

if (!function_exists('array_eq')) {
    /**
     * Notes: 判断两个数组的长度、键值对是否一一对应且相等
     * Author: Uncle-L
     * Date: 2020/10/26
     * Time: 16:56
     * @param array $arr1
     * @param array $arr2
     * @return bool
     */
    function array_eq(array $arr1 = [], array $arr2 = []): bool {
        if (count($arr1) != count($arr2)) return false;
        foreach ($arr1 as $k => $v) {
            if (!isset($arr2[$k]) || $arr2[$k] != $v) return false;
        }
        return true;
    }
}

if (!function_exists('array_in')) {
    /**
     * Notes: 判断数组值是否在指定规则数组内（只针对一维数组）
     * Author: Uncle-L
     * Date: 2020/2/17
     * Time: 14:19
     * @param array $arr [待判断数组]
     * @param array $rule_arr [规则数组]
     * @return bool
     */
    function array_in(array $arr = [], array $rule_arr = []): bool {
        if (!$arr || !$rule_arr || !is_array($arr) || !is_array($rule_arr)) return false;
        if (count($arr) > count($rule_arr)) return false;
        foreach ($arr as $v) {
            if (!in_array($v, $rule_arr)) return false;
        }
        return true;
    }
}

if (!function_exists('array_supp')) {
    /**
     * Notes: 数组补齐
     * Author: Uncle-L
     * Date: 2020/3/3
     * Time: 18:47
     * @param array $arr [待补齐数组]
     * @param array $rule_arr [规则数组]
     * @return array|bool
     */
    function array_supp(array $arr = [], array $rule_arr = []) {
        if (!$arr || !is_array($arr)) return $rule_arr;
        foreach ($rule_arr as $rule_k => $rule_v) {
            if (!isset($arr[$rule_k])) continue;
            $v = $arr[$rule_k];
            if (is_array($rule_v)) {
                // 如果和合并元素
                if (!is_array($v)) continue;
                $v = array_depth($rule_v, 1) ? ($v + $rule_v) : array_supp($v, $rule_v);
            }
            $rule_arr[$rule_k] = $v;
        }
        return $rule_arr;
    }
}

if (!function_exists('array_depth')) {
    /**
     * Notes: 获取/判断数组维度
     * Author: Uncle-L
     * Date: 2020/6/28
     * Time: 18:04
     * @param array $arr
     * @param int $rule_depth [规则维度，不传则返回数组维度|传入则校验数组是否该维度]
     * @return int|bool
     */
    function array_depth(array $arr = [], int $rule_depth = -1) {
        $arrs = new RecursiveIteratorIterator(new RecursiveArrayIterator($arr));
        $depth = 0;
        foreach ($arrs as $v)
            $arrs->getDepth() >= $depth and $depth = $arrs->getDepth();
        return is_positive_integer($rule_depth) ? ++$depth === $rule_depth : ++$depth;
    }
}

if (!function_exists('array_insert')) {
    /**
     * Notes: 在数组指定key前插入数组
     * Author: Uncle-L
     * Date: 2020/7/18
     * Time: 23:46
     * @param array $arr [目标数组]
     * @param array $data [待插入数组]
     * @param bool|mixed $key [指定key]
     * @return array
     */
    function array_insert(array &$arr = [], array $data = [], $key = false): array {
        if (!is_array($data)) return $arr;
        $key = $key === false ? key($arr) : $key;
        if (!isset($arr[$key])) return $arr;
        $arrHead = [];
        $arrFoot = [];
        $headSliceEnd = false;
        foreach ($arr as $k => $v) {
            if (!$headSliceEnd) {
                if ($k == $key) {
                    $headSliceEnd = true;
                    $arrFoot[$k] = $v;
                } else {
                    $arrHead[$k] = $v;
                }
            } else {
                $arrFoot[$k] = $v;
            }
        }
        $arr = $arrHead + $data + $arrFoot;
        return $arr;
    }
}

if (!function_exists('array_key_prefix')) {
    /**
     * Notes: 给数组所有key或指定key增加前缀
     * Author: Uncle-L
     * Date: 2021/1/8
     * Time: 16:37
     * @param array $arr
     * @param string $prefix [前缀]
     * @param bool|mixed $key [指定key，注意：若新key在原数组中存在则前缀翻倍再计算]
     * @return array
     */
    function array_key_prefix(array &$arr, string $prefix, $key = false): array {
        if (!$arr || !$prefix) return $arr;
        if ($key) {
            if (!array_key_exists($key, $arr)) return $arr;
            $newKey = $prefix . $key;
            if (array_key_exists($newKey, $arr)) return array_key_prefix($arr, $prefix . $prefix, $key);
            $arr[$newKey] = $arr[$key];
            unset($arr[$key]);
            return $arr;
        }
        $arr = array_combine(array_map(function ($key) use ($prefix) {
            return $prefix . $key;
        }, array_keys($arr)), $arr);
        return $arr;
    }
}

if (!function_exists('array_not_key')) {
    /**
     * Notes: 数组没有特定声明的key，即常规的没有指定key的数组，例：["apple","pear","orange"]
     * Author: Uncle-L
     * Date: 2021/11/7
     * Time: 23:17
     * @param array $arr
     * @return bool
     */
    function array_not_key(array $arr): bool {
        if (!$arr) return true;
        $keys = array_keys($arr);
        if (end($keys) !== (count($keys) - 1)) return false;
        if (reset($keys) !== 0) return false;
        for ($i = 0; $i < count($keys); $i++)
            if ($i !== $keys[$i]) return false;
        return true;
    }
}

/****************************************************** 验证 ******************************************************/

if (!function_exists('is_mobile')) {
    /**
     * Notes: 是否移动端判断
     * Author: Uncle-L
     * Date: 2020/2/19
     * Time: 9:21
     * @return bool [是移动端返回true|反之]
     */
    function is_mobile(): bool {
        $_SERVER['ALL_HTTP'] = isset($_SERVER['ALL_HTTP']) ?? '';
        $mobile_browser = '0';
        if (preg_match("/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|iphone|ipad|ipod|android|xoom)/i", strtolower($_SERVER['HTTP_USER_AGENT'])))
            $mobile_browser++;
        if ((isset($_SERVER['HTTP_ACCEPT'])) and (strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') !== false))
            $mobile_browser++;
        if (isset($_SERVER['HTTP_X_WAP_PROFILE']))
            $mobile_browser++;
        if (isset($_SERVER['HTTP_PROFILE']))
            $mobile_browser++;
        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac',
            'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno',
            'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-',
            'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-',
            'newt', 'noki', 'oper', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox',
            'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar',
            'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
            'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp',
            'wapr', 'webc', 'winw', 'winw', 'xda', 'xda-'
        );
        if (in_array($mobile_ua, $mobile_agents))
            $mobile_browser++;
        if (strpos(strtolower($_SERVER['ALL_HTTP']), 'operamini') !== false)
            $mobile_browser++;
        // win
        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') !== false)
            $mobile_browser = 0;
        // win7
        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows phone') !== false)
            $mobile_browser++;
        if ($mobile_browser > 0) {
            return true;
        }
        return false;
    }
}

if (!function_exists('is_positive_integer')) {
    /**
     * Notes: 正整数验证
     * Author: Uncle-L
     * Date: 2020/3/21
     * Time: 17:38
     * @param mixed $data
     * @return bool [是正整数返回true|反之]
     */
    function is_positive_integer($data): bool {
        return is_numeric($data) && is_int($data + 0) && ($data + 0) > 0;
    }
}

if (!function_exists('is_positive_int')) {
    /**
     * Notes: 正整数验证
     * Author: Uncle-L
     * Date: 2020/3/21
     * Time: 17:38
     * @param mixed $data
     * @return bool
     */
    function is_positive_int($data): bool {
        return is_scalar($data) && !!preg_match("/^[1-9]\d*$/", $data);
    }
}

if (!function_exists('is_timestamp')) {
    /**
     * Notes: 时间戳验证
     * Author: Uncle-L
     * Date: 2020/3/21
     * Time: 17:38
     * @param mixed $data
     * @return bool
     */
    function is_timestamp($data): bool {
        return is_positive_int($data) && $data == strtotime(date("Y-m-d H:i:s", $data));
    }
}

if (!function_exists('is_json')) {
    /**
     * Notes: json数据格式验证
     * Author: Uncle-L
     * Date: 2020/3/21
     * Time: 17:38
     * @param mixed $data
     * @return bool [是json数据格式返回true|反之]
     */
    function is_json($data): bool {
        json_decode($data);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}

if (!function_exists('is_datetime')) {
    /**
     * Notes: 时间日期有效性验证
     * Author: Uncle-L
     * Date: 2020/5/26
     * Time: 16:53
     * @param mixed $data [待校验的时间日期]
     * @param string $format [需要校验的时间日期格式]
     * @return bool
     */
    function is_datetime($data, string $format = 'Y-m-d H:i:s'): bool {
        $timestamp = strtotime($data);
        if (!$timestamp) return false;
        return date($format, $timestamp) == $data;
    }
}

if (!function_exists('is_md5_result')) {
    /**
     * Notes: 判断是否为md5加密结果
     * Author: Uncle-L
     * Date: 2021/7/2
     * Time: 17:19
     * @param $result
     * @param int $len
     * @return bool
     */
    function is_md5_result($result, int $len = 32): bool {
        if (!is_string($result) || !$result || strlen($result) != $len) return false;
        return !!preg_match("/^[a-z0-9]{32}$/", $result);
    }
}

if (!function_exists('is_positive_int_array')) {
    /**
     * Notes: 判断数组是否为正整数数组
     * Author: Uncle-L
     * Date: 2021/11/15
     * Time: 15:00
     * @param mixed $data
     * @return bool
     */
    function is_positive_int_array($data): bool {
        if (!is_array($data) || !$data) return false;
        $arrStr = implode(",", $data);
        return preg_match("/^[1-9]\d*(?:,[1-9]\d*)*$/", $arrStr);
    }
}

/****************************************************** 时间日期 ******************************************************/

if (!function_exists('msectime')) {
    /**
     * Notes: 获取当前毫秒时间戳
     * Author: Uncle-L
     * Date: 2020/11/23
     * Time: 11:35
     * @return int
     */
    function msectime(): int {
        list($msec, $sec) = explode(' ', microtime());
        $msectime = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
        return (int)substr($msectime, 0, 13);
    }
}

/****************************************************** 文件 ******************************************************/

if (!function_exists('dir_delete')) {
    /**
     * Notes: 目录删除
     * Author: Uncle-L
     * Date: 2021/11/7
     * Time: 13:11
     * @param string $path [要删除的目录地址]
     * @param bool $inc_self [是否包含自身]
     * @param array ...$exclude_path    排除地址
     */
    function dir_delete(string $path, bool $inc_self = false, ...$exclude_path): void {
        if (!is_dir($path)) return;
        $path = str_replace("\\", "/", $path);
        if (substr($path, -1) === "/") $path = substr($path, 0, -1);
        $fileArr = scandir($path);
        $excludePath = isset($exclude_path[0]) && is_array($exclude_path[0]) ? $exclude_path[0] : $exclude_path;
        $excludePath = array_map(function($v)use($path){
            $v = str_replace("\\", "/", $v);
            return str_replace($path."/","",$v);
        },$excludePath);
        foreach ($fileArr as $file) {
            if ($file === "." || $file === "..") continue;
            $filePath = $path . "/{$file}";
            if (is_file($filePath)) {
                if (in_array($file, $excludePath)) continue;
                @unlink($filePath);
            } else {
                dir_delete($filePath, true, $excludePath);
            }
        }
        if ($inc_self) @rmdir($path);
    }
}

if (!function_exists('file_copy')) {
    /**
     * Notes: 文件复制
     * Author: Uncle-L
     * Date: 2021/11/7
     * Time: 14:01
     * @param string $source_path [源地址]
     * @param string $dest_path [目标地址]
     * @throws \Exception
     */
    function file_copy(string $source_path, string $dest_path): void {
        $sourcePath = str_replace("\\", "/", $source_path);
        $destPath = str_replace("\\", "/", $dest_path);
        if (!is_file($sourcePath)) return;
        $destDirPath = dirname($destPath);
        if (is_dir($destDirPath)) {
            $isWriteable = file_is_writeable($destDirPath);
            if (!$isWriteable) throw new \Exception("文件夹[{$destDirPath}]无写入权限");
        } else
            try {
                mkdir($destDirPath, 0755, true);
            } catch (\Exception $e) {
                throw new \Exception("文件夹[{$destDirPath}]不存在，请创建并赋予写入权限后重新执行此操作");
            }
        copy($sourcePath, $destPath);
    }
}

if (!function_exists('dir_copy')) {
    /**
     * Notes: 目录复制
     * Author: Uncle-L
     * Date: 2021/11/7
     * Time: 14:01
     * @param string $source_path [源地址]
     * @param string $dest_path [目标地址]
     * @param array ...$exclude_path 排除地址
     * @throws Exception
     */
    function dir_copy(string $source_path, string $dest_path, ...$exclude_path): void {
        $sourcePath = str_replace("\\", "/", $source_path);
        if (substr($sourcePath, -1) === "/") $sourcePath = substr($sourcePath, 0, -1);
        $destPath = str_replace("\\", "/", $dest_path);
        if (substr($destPath, -1) === "/") $destPath = substr($destPath, 0, -1);
        if (!is_dir($sourcePath)) return;
        if (!is_dir($destPath)) mkdir($destPath, 0755, true);
        $sourceFileArr = scandir($sourcePath);
        $excludePath = isset($exclude_path[0]) && is_array($exclude_path[0]) ? $exclude_path[0] : $exclude_path;
        $excludePath = array_map(function($v)use($sourcePath){
            $v = str_replace("\\", "/", $v);
            return str_replace($sourcePath."/","",$v);
        },$excludePath);
        foreach ($sourceFileArr as $sourceFile) {
            if ($sourceFile === "." || $sourceFile === "..") continue;
            $sourceFilePath = $sourcePath . "/{$sourceFile}";
            $destFilePath = $destPath . "/{$sourceFile}";
            if ($excludePath && in_array($sourceFile, $excludePath)) continue;
            if (is_file($sourceFilePath)) {
                file_copy($sourceFilePath, $destFilePath);
            } else {
                dir_copy($sourceFilePath, $destFilePath);
            }
        }
    }
}

if (!function_exists('file_is_writeable')) {
    /**
     * Notes: 文件/目录 是否可写（取代系统自带的 is_writeable 函数）
     * Author: Uncle-L
     * Date: 2021/11/10
     * Time: 10:14
     * @param string $path [文件/目录 地址]
     * @return bool
     */
    function file_is_writeable(string $path): bool {
        $path = str_replace("\\", "/", $path);
        if (substr($path, -1) === "/") $path = substr($path, 0, -1);
        if (is_dir($path)) {
            $dir = $path;
            $testFile = "{$dir}/test_is_writable_" . date("YmdHis") . ".txt";
            if ($fp = @fopen($testFile, 'w')) {
                @fclose($fp);
                @unlink($testFile);
                $isWiteable = true;
            } else {
                $isWiteable = false;
            }
        } else {
            if ($fp = @fopen($path, 'a+')) {
                @fclose($fp);
                $isWiteable = true;
            } else {
                $isWiteable = false;
            }
        }
        return $isWiteable;
    }
}

if (!function_exists('dir_is_writeable')) {
    /**
     * Notes: 目录是否可写
     * Author: Uncle-L
     * Date: 2021/11/7
     * Time: 14:01
     * @param string $path [目录地址]
     * @return bool
     */
    function dir_is_writeable(string $path): bool {
        $path = str_replace("\\", "/", $path);
        if (substr($path, -1) === "/") $path = substr($path, 0, -1);
        if (!is_dir($path)) return false;
        $isWriteable = file_is_writeable($path);
        if (!$isWriteable) return false;
        $fileArr = scandir($path);
        foreach ($fileArr as $file) {
            if ($file === "." || $file === "..") continue;
            $filePath = "{$path}/{$file}";
            if (is_file($file)) continue;
            $isWriteable = file_is_writeable($filePath);
            if (!$isWriteable) return false;
        }
        return true;
    }
}

if (!function_exists('dir_writeable_mk')) {
    /**
     * Notes: 目录是否可写，不存在则创建
     * 目录存在：
     *      可写：结束执行
     *      不可写：抛出异常提示
     * 目录不存在：
     *      创建，失败则抛出异常提示
     * Author: Uncle-L
     * Date: 2021/11/7
     * Time: 14:01
     * @param string $path [目录地址]
     * @param bool $recursion [递归，是否递归检查子目录]
     * @throws \Exception
     */
    function dir_writeable_mk(string $path, bool $recursion = false): void {
        $path = str_replace("\\", "/", $path);
        if (substr($path, -1) === "/") $path = substr($path, 0, -1);
        if (is_dir($path)) {
            $isWritable = $recursion ? dir_is_writeable($path) : file_is_writeable($path);
            if (!$isWritable) throw new \Exception("请赋予目录[{$path}]写入权限");
            return;
        } else
            try {
                mkdir($path, 0755, true);
            } catch (\Exception $e) {
                throw new \Exception("请创建目录[{$path}]，并赋予其写入权限");
            }
    }
}

/****************************************************** 字符串 ******************************************************/

if (!function_exists('rand_char')) {
    /**
     * Notes: 获取指定长度随机字符串
     * Author: Uncle-L
     * Date: 2020/11/23
     * Time: 11:38
     * @param int $len [默认长度32]
     * @return string
     */
    function rand_char(int $len = 32): string {
        $str = "";
        $strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
        $max = strlen($strPol) - 1;
        for ($i = 0; $i < $len; $i++) {
            $str .= $strPol[rand(0, $max)];
        }
        return $str;
    }
}

if (!function_exists('filter_sql')) {
    /**
     * Notes: 过滤sql，防止sql注入
     * Author: Uncle-L
     * Date: 2021/1/5
     * Time: 18:27
     * @param string $str
     * @return string
     */
    function filter_sql(string $str): string {
        if (!$str) return '';
        $str = addslashes($str);
        $str = str_replace("_", "\_", $str);
        $str = str_replace("%", "\%", $str);
        $str = str_replace(" ", "", $str);
        $str = nl2br($str); // 回车转换
        return htmlspecialchars($str);
    }
}

if (!function_exists('start_with')) {
    /**
     * Notes: 检测字符串是否以指定的字符串开始/指定位置开始
     * Author: Uncle-L
     * Date: 2021/2/4
     * Time: 10:27
     * @param string $str [要检查的字符串]
     * @param string $pattern [指定字符串]
     * @param int $idx [开始位置]
     * @return bool
     */
    function start_with(string $str, string $pattern, int $idx = 0): bool {
        return strpos($str, $pattern) === $idx;
    }
}

if (!function_exists('strnpos')) {
    /**
     * Notes: 找字符串在另一字符串中第n次出现的位置
     * Author: Uncle-L
     * Date: 2021/11/18
     * Time: 15:19
     * @param string $str
     * @param string $find
     * @param int $n
     * @return int|false
     */
    function strnpos(string $str, string $find, int $n) {
        $posVal = false;
        for ($i = 1; $i <= $n; $i++) {
            $pos = strpos($str, $find);
            $str = substr($str, $pos + 1);
            $posVal = $pos + ($posVal === false ? 0 : $posVal) + 1;
        }
        return $posVal ? ($posVal - 1) : false;
    }
}

if (!function_exists('uppercase_to_underline')) {
    /**
     * Notes: 大写格式转换为下划线分割（兼容首字母大小写情况）
     * Author: Uncle-L
     * Date: 2021/4/15
     * Time: 14:04
     * @param string $str
     * @return string
     */
    function uppercase_to_underline(string $str): string {
        return strtolower(preg_replace('/(?<=[a-z])([A-Z])/', '_$1', $str));
    }
}

if (!function_exists('underline_to_uppercase')) {
    /**
     * Notes: 下划线转换为大写格式
     * Author: Uncle-L
     * Date: 2021/4/15
     * Time: 14:04
     * @param string $str
     * @param bool $has_first    是否首字母大写
     * @return string
     */
    function underline_to_uppercase(string $str,bool $has_first = false) : string{
        $strArr = explode('_', $str);
        $res = $has_first?'':$strArr[0];
        for($i = $has_first?0:1,$l = count($strArr);$i<$l;$i++){
            $res .= ucfirst($strArr[$i]);
        }
        return $res;
    }
}

if (!function_exists('calling_class')) {
    /**
     * Notes: 获取调用此方法的类名
     * Author: Uncle-L
     * Date: 2021/4/15
     * Time: 14:15
     * @return mixed
     */
    function calling_class() {
        $trace = debug_backtrace();
        $class = $trace[1]['class'];

        for ($i = 1; $i < count($trace); $i++) {
            if (isset($trace[$i]))
                if ($class != $trace[$i]['class'])
                    return $trace[$i]['class'];
        }
    }
}

if (!function_exists('fuzzy_mobile')) {
    /**
     * Notes: 模糊手机号码（num=<7直接输出，7<num<11前后各保留2位，num>=11前后各保留3、4位，其余字符用*代替）
     * Author: Uncle-L
     * Date: 2021/4/28
     * Time: 11:38
     * @param string|int $num
     * @return string|int
     */
    function fuzzy_mobile($num) {
        $len = strlen($num);
        if ($len <= 7) return $num;
        list($beforeDigit, $afterDigit) = $len < 11 ? [2, 2] : [3, 4];
        return substr($num, 0, $beforeDigit)
            . str_repeat("*", $len - ($beforeDigit + $afterDigit))
            . substr($num, $len - ($afterDigit + 1), $afterDigit);
    }
}

if (!function_exists('exception_to_str')) {
    /**
     * Notes: Exception异常对象转换为字符串显示
     * Author: Uncle-L
     * Date: 2021/10/18
     * Time: 16:11
     * @param Exception $e
     * @return string
     */
    function exception_to_str(\Exception $e): string {
        return get_class($e) . " " . $e->getMessage() . " in " . $e->getFile() . ":" . $e->getLine() . "\r\n"
            . "Stack trace:\r\n" . $e->getTraceAsString();
    }
}

if (!function_exists('pinyin_first_letter')) {
    /**
     * Notes: 获取字符串拼音的第一个字母（大写），不是字母返回空字符串
     * Author: Uncle-L
     * Date: 2021/11/8
     * Time: 14:04
     * @param string $str
     * @return string
     */
    function pinyin_first_letter(string $str): string {
        $str = ltrim($str);
        if (!$str) return "";
        $str = mb_substr($str, 0, 2, "UTF-8");
        $fchar = ord($str{0});
        if ($fchar >= ord('A') && $fchar <= ord('z')) return strtoupper($str{0});
        $s1 = iconv('UTF-8', 'GB2312//IGNORE', $str);   // IGNORE 避免繁体字报错
        $s2 = iconv('GB2312//IGNORE', 'UTF-8', $s1);
        $s = $s2 == $str ? $s1 : $str;
        $asc = ord($s{0}) * 256 + ord($s{1}) - 65536;
        if ($asc >= -20319 && $asc <= -20284) return 'A';
        if ($asc >= -20283 && $asc <= -19776) return 'B';
        if ($asc >= -19775 && $asc <= -19219) return 'C';
        if ($asc >= -19218 && $asc <= -18711) return 'D';
        if ($asc >= -18710 && $asc <= -18527) return 'E';
        if ($asc >= -18526 && $asc <= -18240) return 'F';
        if ($asc >= -18239 && $asc <= -17923) return 'G';
        if ($asc >= -17922 && $asc <= -17418) return 'H';
        if ($asc >= -17417 && $asc <= -16475) return 'J';
        if ($asc >= -16474 && $asc <= -16213) return 'K';
        if ($asc >= -16212 && $asc <= -15641) return 'L';
        if ($asc >= -15640 && $asc <= -15166) return 'M';
        if ($asc >= -15165 && $asc <= -14923) return 'N';
        if ($asc >= -14922 && $asc <= -14915) return 'O';
        if ($asc >= -14914 && $asc <= -14631) return 'P';
        if ($asc >= -14630 && $asc <= -14150) return 'Q';
        if ($asc >= -14149 && $asc <= -14091) return 'R';
        if ($asc >= -14090 && $asc <= -13319) return 'S';
        if ($asc >= -13318 && $asc <= -12839) return 'T';
        if ($asc >= -12838 && $asc <= -12557) return 'W';
        if ($asc >= -12556 && $asc <= -11848) return 'X';
        if ($asc >= -11847 && $asc <= -11056) return 'Y';
        if ($asc >= -11055 && $asc <= -10247) return 'Z';
        return "";
    }
}

if (!function_exists('aes_encrypt')) {
    /**
     * Notes: AES数据加密
     * Author: Uncle-L
     * Date: 2021/12/27
     * Time: 16:07
     * @param string $data [源数据]
     * @param string $key [16字节密钥]
     * @param string $iv [16字节初始向量]
     * @return string
     */
    function aes_encrypt(string $data, string $key, string $iv): string {
        return base64_encode(openssl_encrypt($data, "AES-128-CBC", $key, 0, $iv));
    }
}

if (!function_exists('aes_decrypt')) {
    /**
     * Notes: AES数据解密
     * Author: Uncle-L
     * Date: 2021/12/27
     * Time: 16:07
     * @param string $encrypted [密文]
     * @param string $key [16字节密钥]
     * @param string $iv [16字节初始向量]
     * @return string
     */
    function aes_decrypt(string $encrypted, string $key, string $iv): string {
        return openssl_decrypt(base64_decode($encrypted), "AES-128-CBC", $key, 0, $iv);
    }
}

if (!function_exists('rsa_encrypt')) {
    /**
     * Notes: RSA数据加密
     * Author: Uncle-L
     * Date: 2021/12/27
     * Time: 16:10
     * @param string $data [源数据]
     * @param string $publicKey [公钥]
     * @return string
     */
    function rsa_encrypt(string $data, string $publicKey): string {
        openssl_public_encrypt($data, $encrypted, $publicKey);
        return base64_encode($encrypted);
    }
}

if (!function_exists('rsa_decrypt')) {
    /**
     * Notes: RSA数据解密
     * Author: Uncle-L
     * Date: 2021/12/27
     * Time: 16:13
     * @param string $encrypted [密文]
     * @param string $privateKey [私钥]
     * @return string
     */
    function rsa_decrypt(string $encrypted, string $privateKey): string {
        openssl_private_decrypt(base64_decode($encrypted), $decrypted, $privateKey);
        return $decrypted;
    }
}

if (!function_exists('rsa_sign')) {
    /**
     * Notes: RSA数据签名
     * Author: Uncle-L
     * Date: 2021/12/27
     * Time: 16:14
     * @param string $data [源数据]
     * @param string $privateKey [私钥]
     * @return string
     */
    function rsa_sign(string $data, string $privateKey): string {
        openssl_sign($data, $signature, $privateKey);
        return base64_encode($signature);
    }
}

if (!function_exists('rsa_sign_verify')) {
    /**
     * Notes: RSA数据验签
     * Author: Uncle-L
     * Date: 2021/12/27
     * Time: 16:15
     * @param string $data [源数据]
     * @param string $sign [签名]
     * @param string $publicKey [公钥]
     * @return bool
     */
    function rsa_sign_verify(string $data, string $sign, string $publicKey): bool {
        return (bool)openssl_verify($data, base64_decode($sign), $publicKey);
    }
}