<?php

namespace yunj\control\field;

abstract class YunjField {

    protected $requireArgs = [
        'type' => 'text',        // 类型（默认text）
        'title' => '',           // 标题（可选）
        'default' => '',         // 默认值（可选，如：['read']）
        'desc' => '',            // 描述（可选）
        'verify' => '',          // 验证规则（可选，例：'require|mobile'，必填且为手机格式）
        'verifyTitle' => '',     // 验证标题（可选）
    ];

    /**
     * 字段key
     * @var string
     */
    protected $key;

    /**
     * 字段配置
     * @var array
     */
    protected $args;

    /**
     * Notes: 设置属性
     * Author: Uncle-L
     * Date: 2020/8/18
     * Time: 10:10
     * @param $key
     * @param $args
     * @return $this
     */
    public function setAttr($key, $args) {
        $this->key = $key;
        $this->setArgs($args);
        return $this;
    }

    /**
     * Notes: 设置字段配置
     * Author: Uncle-L
     * Date: 2021/11/19
     * Time: 18:33
     * @param $args
     */
    final protected function setArgs($args): void {
        $args += $this->defineExtraArgs() + $this->requireArgs;
        $args = $this->handleArgs($args);
        $this->args = $args;
    }

    /**
     * Notes: 定义额外参数
     * Author: Uncle-L
     * Date: 2021/11/19
     * Time: 17:57
     * @return array
     */
    protected function defineExtraArgs(): array {
        return [];
    }

    /**
     * Notes: 处理args
     * Author: Uncle-L
     * Date: 2020/12/3
     * Time: 10:22
     * @param $args
     * @return array
     */
    protected function handleArgs($args): array {
        return $args;
    }

    /**
     * Notes: 返回字段配置
     * Author: Uncle-L
     * Date: 2021/11/19
     * Time: 18:34
     * @return array
     */
    public function args(): array {
        return $this->args;
    }

}