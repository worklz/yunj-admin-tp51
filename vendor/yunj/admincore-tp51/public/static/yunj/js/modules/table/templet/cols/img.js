/**
 * TableColImg
 */
layui.define(['TableColTemplet', 'jquery', 'yunj'], function (exports) {

    let TableColTemplet = layui.TableColTemplet;
    let win = window;
    let doc = document;
    let $ = layui.jquery;

    class TableColImg extends TableColTemplet {

        constructor(options) {
            super(options);
        }

        layout() {
            let that = this;
            return `{{# 
                         let src = d.${that.key};
                     }}
                    {{#  if(d.is_export){  }}
                    {{ src }}
                    {{#  }else{  }}
                    {{#  
                            let imgOnrrror = "";
                            let defaultSrc = "${that.args.default}";
                            if(defaultSrc) imgOnrrror = "this.src='"+defaultSrc+"';this.onerror=null;";
                    }}
                            <img class="table-row-img" src="{{ src?src:defaultSrc }}" alt="" title="点击预览" onerror="{{ imgOnrrror }}">
                    {{#  }  }}`;
        }

        defineExtraEventBind() {
            let that = this;

            // 防止重复绑定事件
            if(yunj.isUndefined(win.TABLE_ROW_IMG_CLICK_PREVIEW_EVENT_BIND)){
                win.TABLE_ROW_IMG_CLICK_PREVIEW_EVENT_BIND = true;
                $(doc).on('click', '.table-row-img', function (e) {
                    let src = $(this).attr('src');
                    if (!src) return false;
                    yunj.previewImg(src);
                    e.stopPropagation();
                });
            }
        }

    }

    exports('TableColImg', TableColImg);
});