<?php

namespace yunj\enum;

final class UpgradeType extends Enum {

    // 合并升级
    const MERGE = 11;

    // 重置为当前最新
    const RESET = 22;

    // 不升级
    const NO = 99;

}