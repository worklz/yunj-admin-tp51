<?php

namespace yunj\builder;

use yunj\Config;
use yunj\Validate;
use yunj\enum\BuilderType;
use yunj\control\YunjControl;

final class YunjForm extends Builder {

    protected $type = BuilderType::FORM;

    protected $builderValidateClass = "\\yunj\\validate\\YunjForm";

    protected $scriptFileList = ["/static/yunj/js/form.min.js?v=" . YUNJ_VERSION];

    protected $options = ["tab", "url", "field", "fieldValidate", "button", "load", "submit"];

    /**
     * 表单切换栏（默认选中第一项）
     * 'tab'=>[
     *      'base'=>'基础',
     *      'other'=>'其他'
     *  ]
     * @var array<key,title>
     */
    private $tab = [];

    /**
     * 异步数据接口地址（默认当前地址）
     * "url"=>"xxx.com/base.html"
     * @var string
     */
    private $url;

    /**
     * 表单字段
     * @var array<field-key,field-args>|array<tab-key,array<field-key,field-args>>
     * 示例：tab没有设置时   array<field-key,field-args>
     * 'field'=>[
     *      // 单行文本框
     *      'field_key'=>[
     *          'type'=>'text',         //（可选，默认text）
     *          'title'=>'field_label', //（可选）
     *          'placeholder'=>'',      //（可选）
     *          'value'=>'',            //默认值（可选）
     *      ],...
     *  ]
     * 示例：tab设置时    array<tab-key,array<field-key,field-args>>
     * 'field'=>[
     *      'base'=>[],     // 切换栏tab=base的筛选表单（可选）
     *      'other'=>[      // 切换栏tab=other的筛选表单（可选）
     *          // 单行文本框
     *          'field_key'=>[
     *              'type'=>'text',         //（可选，默认text）
     *              'title'=>'field_label', //（可选）
     *              'placeholder'=>'',      //（可选）
     *              'value'=>'',            //默认值（可选）
     *          ],...
     *      ]
     *  ]
     */
    private $field = [];

    /**
     * 表单字段验证器的回调方法
     * @var callable
     */
    private $fieldValidateCallback;

    /**
     * 表格按钮（默认为空）
     * 'button'=>['back','reload','clear','submit']
     * @var array<string>
     */
    private $button;

    /**
     * 数据加载回调方法
     * @var callable
     */
    private $loadCallback;

    /**
     * 数据提交回调方法
     * @var callable
     */
    private $submitCallback;

    protected function setAttr(array $args = []): void {
        parent::setAttr($args);
        $this->config = Config::get('form.', []);
    }

    /**
     * Notes: 切换栏
     * Author: Uncle-L
     * Date: 2021/3/29
     * Time: 10:05
     * @param callable|array<key,title>|string|number $code
     * callable:
     *      function(){
     *          return ["base"=>"基础","other"=>"其他"];
     *      }
     * array<key,title>:
     *      ["base"=>"基础","other"=>"其他"];
     * @param string $title
     * @return YunjForm
     */
    public function tab($code, string $title = '') {
        if ($this->existError()) return $this;
        if (!is_callable($code)) {
            $callable = function () use ($code, $title) {
                return is_array($code) ? $code : [$code => $title];
            };
        } else {
            $callable = $code;
        }
        $this->setOptionCallbale('tab', $callable);
        return $this;
    }

    /**
     * Notes: 执行切换栏回调
     * Author: Uncle-L
     * Date: 2021/3/29
     * Time: 10:42
     * @param callable $callable
     * @return YunjForm
     */
    protected function exec_tab_callable(callable $callable) {
        $tab = $this->tab ? $this->tab : [];
        $result = call_user_func($callable);
        if (!is_array($result)) return $this->error("YunjForm [tab] 需返回有效数组");
        $this->tab = $result + $tab;
        return $this;
    }

    /**
     * Notes: 判断是否设置tab
     * Author: Uncle-L
     * Date: 2022/3/8
     * Time: 17:30
     * @return bool
     */
    private function isSetTab(): bool {
        $this->execOptionsCallbale("tab");
        return $this->tab && count($this->tab) > 0;
    }

    /**
     * Notes: 请求地址
     * Author: Uncle-L
     * Date: 2021/3/29
     * Time: 10:41
     * @param callable|string $url
     * callable:
     *      function(){
     *          return "xxxx.com/xxx.html";
     *      }
     * string:
     *      xxxx.com/xxx.html
     * @return YunjForm
     */
    public function url($url) {
        if ($this->existError()) return $this;
        if (!is_callable($url)) {
            $callable = function () use ($url) {
                return $url;
            };
        } else {
            $callable = $url;
        }
        $this->setOptionCallbale('url', $callable);
        return $this;
    }

    /**
     * Notes: 执行请求地址回调
     * Author: Uncle-L
     * Date: 2021/10/8
     * Time: 18:33
     * @param callable $callable
     * @return YunjForm
     */
    protected function exec_url_callable(callable $callable) {
        if ($this->existError()) return $this;
        $url = $callable();
        if (!is_string($url)) return $this->error("YunjForm [url] 需返回有效地址");
        $this->url = $url;
        return $this;
    }

    /**
     * Notes: 表单字段
     * Author: Uncle-L
     * Date: 2021/3/29
     * Time: 10:08
     * @param callable|array<field-key,field-args> $field
     * @return YunjForm
     */
    public function field($field) {
        if ($this->existError()) return $this;
        if (!is_callable($field)) {
            $callable = function () use ($field) {
                return $field;
            };
        } else {
            $callable = $field;
        }
        $this->setOptionCallbale('field', $callable);
        return $this;
    }

    /**
     * Notes: 执行表单字段回调
     * Author: Uncle-L
     * Date: 2021/3/29
     * Time: 10:10
     * @param callable $callable
     * @return YunjForm
     */
    protected function exec_field_callable(callable $callable) {
        if ($this->existError()) return $this;
        $tabs = $this->tab;
        $field = $this->field ?: [];
        if ($tabs) {
            foreach ($tabs as $tab => $title) {
                $tabField = $callable($tab);
                if (!is_array($tabField)) return $this->error("YunjForm [field] 需返回有效数组");
                $field[$tab] = $field[$tab] ?? [];
                $this->handleFieldTab($field[$tab], $tabField);
            }
        } else {
            $_field = $callable(null);
            if (!is_array($_field)) return $this->error("YunjForm [field] 需返回有效数组");
            $this->handleFieldTab($field, $_field);
        }
        $this->field = $field;
        return $this;
    }

    /**
     * Notes: 表单字段的验证器
     * Author: Uncle-L
     * Date: 2021/3/29
     * Time: 10:05
     * @param callable|string<Validate-class-name> $fieldValidate
     * callable:
     *      function(){
     *          // 返回验证器全限定类名
     *          return \\demo\\validate\\TestValidate::class;
     *          // 返回验证器示例
     *          return new TestValidate();
     *      }
     * string:
     *      \\demo\\validate\\TestValidate::class
     * @return YunjForm
     */
    public function fieldValidate($fieldValidate) {
        if ($this->existError()) return $this;
        if (!is_callable($fieldValidate)) {
            $callable = function () use ($fieldValidate) {
                return $fieldValidate;
            };
        } else {
            $callable = $fieldValidate;
        }
        $this->fieldValidateCallback = $callable;
        return $this;
    }

    /**
     * Notes: 处理切换栏对应的表单字段
     * Author: Uncle-L
     * Date: 2021/3/29
     * Time: 11:25
     * @param array $fieldTab
     * @param array $fields
     * @return YunjForm
     */
    protected function handleFieldTab(array &$fieldTab, array $fields) {
        foreach ($fields as $key => $field) {
            if (!$field || !is_array($field)) return $this->error("YunjForm [field] 返回字段[{$key}]异常");
            $fieldTab[$key] = YunjControl::field($key, $field)->args();
        }
        return $this;
    }

    /**
     * Notes: 表单按钮
     * Author: Uncle-L
     * Date: 2021/3/29
     * Time: 10:12
     * @param callable|array<string>|...string $button
     * callable:
     *      function(){
     *          return ["back","reset","submit"];
     *      }
     * array<string>:
     *      ["back","reset","submit"]
     * ...string:
     *      "back","reset","submit"
     * @return YunjForm
     */
    public function button(...$button) {
        if ($this->existError()) return $this;
        if (!$button) return $this;
        $btn0 = $button[0];
        if (is_callable($btn0)) {
            $callable = $btn0;
        } elseif (is_array($btn0)) {
            $callable = function () use ($btn0) {
                return $btn0;
            };
        } else {
            $callable = function () use ($button) {
                return $button;
            };
        }
        $this->setOptionCallbale('button', $callable);
        return $this;
    }

    /**
     * Notes: 执行表单按钮回调
     * Author: Uncle-L
     * Date: 2021/10/8
     * Time: 18:33
     * @param callable $callable
     * @return YunjForm
     */
    protected function exec_button_callable(callable $callable) {
        if ($this->existError()) return $this;
        $this->button = $this->button ?: [];
        $button = $callable();
        if (!is_array($button)) return $this->error("YunjForm [button] 需返回有效数组");
        if (!array_in($button, $this->config['button']))
            return $this->error("YunjForm [button] 返回值不在有效范围=>" . json_encode($this->config['button']));
        $this->button = array_keys(array_flip($this->button) + array_flip($button));
        return $this;
    }

    /**
     * Notes: 数据获取
     * Author: Uncle-L
     * Date: 2021/3/29
     * Time: 10:45
     * @param callable $callable
     * @return YunjForm
     */
    public function load(callable $callable) {
        if ($this->existError()) return $this;
        $this->loadCallback = $callable;
        return $this;
    }

    /**
     * Notes: 数据提交
     * Author: Uncle-L
     * Date: 2021/3/29
     * Time: 10:45
     * @param callable $callable
     * @return YunjForm
     */
    public function submit(callable $callable) {
        if ($this->existError()) return $this;
        $this->submitCallback = $callable;
        return $this;
    }

    /**
     * Notes: 视图配置
     * Author: Uncle-L
     * Date: 2021/10/8
     * Time: 16:32
     * @return array
     * @throws \Exception
     */
    public function viewArgs() {
        $args = parent::viewArgs();
        if ($this->tab) {
            $args['tab'] = $this->tab;
        }
        if ($this->url) {
            $args['url'] = $this->url;
        }
        if ($this->field) {
            $args['field'] = $this->field;
            $args['load'] = !!$this->loadCallback;
        }
        if ($this->button) {
            $args['button'] = $this->button;
        }
        return $args;
    }

    /**
     * Notes: 异步处理数据加载
     * Author: Uncle-L
     * Date: 2020/7/8
     * Time: 13:37
     * @return array
     */
    protected function async_load() {
        $response = call_user_func($this->loadCallback);
        return $response;
    }

    /**
     * Notes: 异步处理数据提交
     * Author: Uncle-L
     * Date: 2020/7/8
     * Time: 13:37
     * @param $param
     * @return array
     */
    protected function async_submit($param) {
        $this->execOptionsCallbale("submit");
        if (!$this->submitCallback) throw_error_json("YunjForm [submit] 未设置");
        // 验证
        $this->async_submit_validate($param);
        // 执行提交动作
        $response = call_user_func_array($this->submitCallback, [$param['data']]);
        return $response;
    }

    /**
     * Notes: 异步处理数据提交验证
     * Author: Uncle-L
     * Date: 2020/9/17
     * Time: 11:51
     * @param $param
     * @return array
     */
    private function async_submit_validate(array &$param): array {
        $this->execOptionsCallbale("tab,field,fieldValidate");
        if ($this->existError()) throw_error_json($this->getError());
        if (!$this->field) return $param;
        $validate = $this->fieldValidateCallback ? call_user_func($this->fieldValidateCallback) : (new Validate());
        if (is_string($validate)) $validate = class_exists($validate) ? (new $validate()) : null;
        if (!$validate || !($validate instanceof Validate)) throw_error_json("YunjForm [fieldValidate] 需返回 \\yunj\\Validate及其子类 的实例对象或全限定类名");

        $fields = [];
        if ($this->isSetTab()) {
            foreach ($this->field as $tab => $tab_fields) $fields += $tab_fields;
        } else {
            $fields = $this->field;
        }
        $res = form_data_validate($validate, $fields, $param["data"], "submit");
        if (!$res) throw_error_json($validate->getError());
        return $param;
    }

}