<?php

namespace yunj\control\field;

class Color extends YunjField {

    private static $instance;

    public static function instance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs(): array {
        return [
            'disabled' => false,     // 禁用
        ];
    }

    protected function handleArgs($args): array {
        if (!strstr($args["verify"], "hexColor"))
            $args["verify"] .= ($args["verify"] ? "|" : "") . "hexColor";
        return $args;
    }

}