# 枚举

#### 使用示例

```php
class OrderStatus extends Enum {
    // 待付款
    const WAIT_PAYMENT = 0;
    // 待发货
    const WAIT_SHIP = 1;
    // 待收货
    const WAIT_RECEIPT = 2;
    // 待评价
    const WAIT_COMMENT = 3;

    public static function getClassOptions() {
        return [
            self::WAIT_COMMENT => "123",
            self::WAIT_SHIP => "456",
        ];
    }

    public function getClass() {
        return $this->match(self::getClassOptions());
    }

}

function test(OrderStatus $orderStatus) {
    var_dump($orderStatus->getValue());
    var_dump($orderStatus->getClass());
    var_dump($orderStatus::getClassOptions());
}

test(OrderStatus::WAIT_COMMENT());
```