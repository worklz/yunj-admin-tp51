<?php

namespace app\demo\controller\import_demo;

use app\demo\controller\Controller;
use app\demo\service\import_demo\Index as Service;

class Index extends Controller {

    public function chain() {
        $builder = YI('demo')
            ->sheet(Service::getInstance()->sheet())
            ->cols(function ($state) {
                return Service::getInstance()->cols($state);
            })
            ->row(function ($sheet,$rowData) {
                return "演示数据不能进行导入操作";
            });
        $builder->assign($this);
        return $this->fetch();
    }

    public function arrayConfig() {
        $builder = YI('demo', [
            "sheet" => Service::getInstance()->sheet(),
            "cols" => function ($sheet) {
                return Service::getInstance()->cols($sheet);
            },
            "row" => function ($sheet,$rowData) {
                return "演示数据不能进行导入操作";
            }
        ]);
        $builder->assign($this);
        return $this->fetch();
    }

}