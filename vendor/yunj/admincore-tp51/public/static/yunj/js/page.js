/**
 * 云静Admin父页面
 */
layui.use(['jquery', 'yunj', 'layer', 'element', 'md5'], function () {
    let win = window;
    let doc = document;
    let $ = layui.jquery;
    let layer = layui.layer;
    let element = layui.element;
    let md5 = layui.md5;

    class YunjPage {

        constructor() {
            this.version = '1.0.0';

            // 页面选项卡filter标识
            this.pageTabFilter = 'xbs_tab';

            this.leftNavEl = null;
            // 左侧菜单栏展开页面内容遮罩层
            this.leftNavOpenPageContentMask = null;

            this.iframeTabBoxEl = null;

            // tab关闭面板
            this.iframeTabClosePanelEl = null;

            // tab关闭面板内容遮罩层
            this.iframeTabClosePanelContentMask = null;

            this.init();
        }

        init() {
            let that = this;
            that.initAttr();
            that.setEventBind();
        }

        initAttr() {
            let that = this;
            that.leftNavEl = $('.left-nav');
            that.leftNavOpenPageContentMask = $('#left_nav_open_page_content_mask');
            that.iframeTabBoxEl = $(".page-content .layui-tab-title");
            that.iframeTabClosePanelEl = $("#tab_close_panel");
            that.iframeTabClosePanelContentMask = $("#tab_close_panel_content_mask");
        }

        leftNavOpen() {
            let that = this;
            let leftNavEl = that.leftNavEl;
            leftNavEl.animate({width: '220px'}, 100);
            $('.page-content').animate({left: '220px'}, 100);
            $('.left-nav i').css('font-size', '14px');
            $('.left-nav cite,.left-nav .li-icon-right').show();
            if ($(win).width() < 768) that.leftNavOpenPageContentMask.show();
        }

        leftNavClose() {
            let that = this;
            let leftNavEl = that.leftNavEl;
            $('.left-nav .open').click();
            $('.left-nav i').css('font-size', '18px');
            leftNavEl.animate({width: '60px'}, 100);
            $('.left-nav cite,.left-nav .li-icon-right').hide();
            $('.page-content').animate({left: '60px'}, 100);
            if ($(win).width() < 768) that.leftNavOpenPageContentMask.hide();
        }

        setEventBind() {
            let that = this;

            // 菜单下的li点击时触发
            that.leftNavEl.on('click', '#nav li', function (e) {
                let liEl = $(this);
                //当菜单收起来时点击打开
                if (that.leftNavEl.width() < 200) that.leftNavOpen();

                //去除所有a标签的active
                that.leftNavEl.find('a').removeClass('active');
                //添加当前li下一层的子元素a，添加active
                liEl.children('a').addClass('active');

                //如果当前li下有子菜单则展开
                if (liEl.children('.sub-menu').length) {
                    if (liEl.hasClass('open')) {
                        // 关闭
                        liEl.removeClass('open');
                        liEl.find('.li-icon-right').removeClass("layui-icon-down").addClass("layui-icon-left");
                        liEl.children('.sub-menu').stop(true, true).slideUp();
                        liEl.siblings().children('.sub-menu').slideUp();
                    } else {
                        // 展开
                        liEl.addClass('open');
                        liEl.children('a').find('.li-icon-right').removeClass("layui-icon-left").addClass("layui-icon-down");
                        liEl.children('.sub-menu').stop(true, true).slideDown();
                        liEl.siblings().children('.sub-menu').stop(true, true).slideUp();
                        liEl.siblings().find('.li-icon-right').removeClass("layui-icon-down").addClass("layui-icon-left");
                        liEl.siblings().removeClass('open');
                    }
                }
                e.stopPropagation();
            });

            // 左边菜单收起来时，鼠标移上去显示提示框的索引index
            that.leftNavEl.on('mouseenter', '#nav .left-nav-li', function (e) {
                let liEl = $(this);
                if (that.leftNavEl.width() < 200) liEl.attr("lay-tips-index", layer.tips(liEl.attr('lay-tips'), liEl));
            });

            //左边菜单收起来时，鼠标移开删除对应索引index的显示提示框
            that.leftNavEl.on('mouseout', '#nav .left-nav-li', function (e) {
                let liEl = $(this);
                let idx = liEl.attr("lay-tips-index");
                if (idx) {
                    layer.close(idx);
                    liEl.attr("lay-tips-index", 0);
                }
            });

            //顶部控制菜单栏的展开和收起
            $('.container .left-open i').click(function (e) {
                that.leftNavEl.width() > 200 ? that.leftNavClose() : that.leftNavOpen();
            });

            // 移动端菜单展开
            that.leftNavEl.on("click", function (e) {
                if ($(win).width() >= 768 || that.leftNavEl.width() > 200 || $(e.target).attr("class") !== that.leftNavEl.attr("class")) return;
                that.leftNavOpen();
            });

            // 移动端菜单关闭
            $(doc).on("click", function (e) {
                if ($(win).width() >= 768 || that.leftNavEl.width() < 200 || $(e.target).attr("id") !== that.leftNavOpenPageContentMask.attr("id")) return;
                that.leftNavClose();
            });

            // 去掉默认的鼠标contextmenu事件，否则会和右键事件同时出现
            that.iframeTabBoxEl.on('contextmenu', 'li', function (e) {
                e.preventDefault();
            });

            // 鼠标点击选项卡事件
            that.iframeTabBoxEl.on('mousedown', 'li', function (e) {
                let liEl = $(this);
                switch (e.button) {
                    case 0:
                        // 点了左键
                        break;
                    case 1:
                        // 点了滚轮，关闭
                        liEl.find('.layui-tab-close').click();
                        break;
                    case 2:
                        // 点了右键，显示面板
                        let tabLeft = liEl.position().left;
                        let tabWidth = liEl.width();
                        let idx = liEl.attr('lay-id');
                        that.iframeTabClosePanelEl.css({'left': tabLeft + tabWidth / 2}).show().attr("lay-id", idx || null);
                        that.iframeTabClosePanelContentMask.show();
                        break;
                }
                e.stopPropagation();
            });

            // 点击关闭当前/其它/全部时触发
            that.iframeTabClosePanelEl.on('click', 'dd', function (e) {
                let iframeTabBoxEl = that.iframeTabBoxEl;
                let type = $(this).attr('data-type');
                let layId = that.iframeTabClosePanelEl.attr('lay-id');
                let closeLiEl = null;
                if (type === 'this' && layId) {
                    closeLiEl = iframeTabBoxEl.find(`li[lay-id=${layId}]`);
                } else if (type === 'other') {
                    iframeTabBoxEl.find('li').eq(0).find('.layui-tab-close').remove();
                    closeLiEl = iframeTabBoxEl.find(layId ? `li[lay-id!=${layId}]` : 'li[lay-id]');
                } else if (type === 'all') {
                    closeLiEl = iframeTabBoxEl.find('li[lay-id]');
                }
                closeLiEl && closeLiEl.find('.layui-tab-close').click();
                that.iframeTabClosePanelEl.hide();
                that.iframeTabClosePanelContentMask.hide();
            });

            // 点击其他地方时隐藏关闭当前/其它/全部时触发面板
            $('.page-content,#tab_close_panel_content_mask,.container,.left-nav').click(function (e) {
                that.iframeTabClosePanelEl.hide();
                that.iframeTabClosePanelContentMask.hide();
            });

            // 设置主题
            $('.set-theme').on('click', function (e) {
                layer.open({
                    type: 5,
                    title: '主题设置',
                    shade: 0.2,
                    area: ['295px', '90%'],
                    offset: 'rb',
                    maxmin: true,
                    shadeClose: true,
                    closeBtn: false,
                    anim: 2,
                    content: $('#theme_tpl').html(),
                    success: function (layero, index) {
                        yunj.theme.set_theme_tpl_active();
                        $('.theme-box li').on('click', function () {
                            yunj.theme.set($(this).data('code'));
                            yunj.close(index);
                        });
                    }
                });
                return false;
            });

            // 监听tab切换
            element.on(`tab(${that.pageTabFilter})`, function () {
                let layId = this.getAttribute('lay-id');
                if (!layId) return;
                let iframe = $(doc).find(`iframe[tab-id=${layId}]`);
                if (iframe.length <= 0) return;
                let tabPageWin = iframe[0].contentWindow;
                try {
                    // 防止跨域请求异常错误抛出
                    // 处理 table 重新resize大小
                    // 用于其他页面调用原页面table.render时大小发生变化
                    let table = tabPageWin.yunj.table;
                    for (let id in table) {
                        if (!table.hasOwnProperty(id)) continue;
                        let tableObj = table[id];
                        tableObj.table.resize(tableObj.table.config.id);
                    }
                } catch (err) {

                }
            });
        }

        /**
         * 页面id
         * @param url
         */
        pageId(url) {
            let that = this;
            url = url.substr(0, 4) === 'http' ? url : location.protocol + "//" + location.host + url;
            return md5(url);
        }

        /**
         * 打开tab子页面
         * @param {string|object} url              页面地址
         * object:{
         *      url:"",
         *      title:"",
         *      rawPage:""
         * }
         * @param {string} title     页面标题
         * @param {string|null} rawPage    源页面标识
         * @return {void}
         */
        openTab(url, title = "", rawPage = null) {
            let that = this;
            let args = yunj.objSupp(yunj.isObj(title) ? title : {
                url: url,
                title: title,
                rawPage: rawPage
            }, {
                url: "",
                title: "",
                rawPage: null
            });
            let tabFilter = that.pageTabFilter;
            let tabId = that.pageId(args.url);

            //判断点击页面是否已打开
            for (let i = 0, len = $('.x-iframe').length; i < len; i++) {
                if ($('.x-iframe').eq(i).attr('tab-id') === tabId) {
                    element.tabChange(tabFilter, tabId);
                    return;
                }
            }

            // 关掉弹窗页面
            layer.closeAll();

            // 判断是否携带源页面标识
            if (args.rawPage) args.url = yunj.urlPushParam(args.url, "rawPage", args.rawPage);

            //新增tab iframe子页面
            element.tabAdd(tabFilter, {
                title: args.title,
                content: `<iframe tab-id="${tabId}" frameborder="0" src="${args.url}" scrolling="yes" class="x-iframe"></iframe>`,
                id: tabId
            });

            //切换选中
            element.tabChange(tabFilter, tabId);
            that.iframeTabBoxEl.find(`li[lay-id=${tabId}] .layui-tab-close`).unbind('click').on('click', function (e) {
                that.closeTab(tabId);
                e.stopPropagation();
            });
        };

        /**
         * 打开popup子页面
         * @param {string|object} url              页面地址
         * object:{
         *      url:"",
         *      title:"",
         *      rawPage:"",
         *      w:null,
         *      h:null
         * }
         * @param {string} title     页面标题
         * @param {string|null} rawPage    源页面标识
         * @param {string|int|null} w       [指定宽]（可选，可设置百分比或者像素，像素传入int）
         * @param {string|int|null} h       [指定高]（可选，同上）
         * @return {void}
         */
        openPopup(url, title = "", rawPage = null, w = null, h = null) {
            let that = this;
            let args = yunj.objSupp(yunj.isObj(title) ? title : {
                url: url,
                title: title,
                rawPage: rawPage,
                w: w,
                h: h
            }, {
                url: "",
                title: "",
                rawPage: null,
                w: null,
                h: null
            });


            let area = 'auto';
            if (args.w !== null && h !== null) {
                args.w = args.w.toString();
                args.h = args.h.toString();
                area = [args.w.indexOf('%') !== -1 ? args.w : `${args.w}px`, args.h.indexOf('%') !== -1 ? args.h : `${args.h}px`];
            } else {
                area = ['80%', '80%'];
            }
            let param = {isPopup: "yes"};
            args.url = yunj.urlPushParam(args.url, param);
            let layerArgs = {
                id: that.pageId(url),
                type: 2,
                title: args.title,
                content: args.rawPage ? yunj.urlPushParam(args.url, "rawPage", args.rawPage) : args.url,
                skin: "popup-page",
                area: area,
                closeBtn: 1,
                shadeClose: true,
                shade: 0.2,
                maxmin: true,
                success: function (layero, index) {
                    //弹出后的回调
                },
                end: function () {
                    //销毁后的回调
                }
            };
            // 关掉其他的弹窗页面
            layer.closeAll();
            return layer.open(layerArgs);
        };

        /**
         * 关闭指定tabId子页面（兼容移动端关闭后显示空白的问题）
         * @param tabId
         */
        closeTab(tabId) {
            let that = this;
            let currIdx = that.iframeTabBoxEl.find(`li[lay-id=${tabId}]`).index();
            let tabLiLen = that.iframeTabBoxEl.find("li").length;
            let showIdx = currIdx < (tabLiLen - 1) ? currIdx + 1 : currIdx - 1;
            that.iframeTabBoxEl.find('li').eq(showIdx).click();
            element.tabDelete(that.pageTabFilter, tabId);
        }

        /**
         * 重定向到指定tab子页面
         * @param tabId    [默认null指向首页]
         * @returns {*}
         */
        redirectTab(tabId = null) {
            let that = this;
            that.iframeTabBoxEl.find(`li${tabId === null ? '.home' : '[lay-id=' + tabId + ']'}`).click();
            return tabId;
        }

        // 当前显示tab页面的page_id
        currTabPageId() {
            let that = this;
            let ttlEl = that.iframeTabBoxEl.find('li.layui-this');
            if (ttlEl.length <= 0) return null;
            let layID = ttlEl.eq(0).attr('lay-id');
            return layID && yunj.isString(layID) ? layID : null;
        }

        // 当前显示popup页面的page_id
        currPopupPageId(popup_page_el = null) {
            let that = this;
            popup_page_el = popup_page_el || $(".layui-layer-iframe.popup-page");
            if (popup_page_el.length <= 0) return null;
            return popup_page_el.find(".layui-layer-content").eq(0).attr("id");
        }

    }

    win.yunj.page = new YunjPage();

});