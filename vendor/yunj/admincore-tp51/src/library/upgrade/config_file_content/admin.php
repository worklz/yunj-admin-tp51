<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2020 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1732983738@qq.com>
// +----------------------------------------------------------------------
// | 后台常规配置，默认文件内容
// +----------------------------------------------------------------------

return [
    "title" => <<<INFO
    // 后台系统标题
    'title' => '云静Admin TP5.1',
INFO
    ,
    "keywords" => <<<INFO
    // 后台系统关键字
    'keywords' => '云静Admin,后台,yunj,admin,thinkphp',
INFO
    ,
    "description"=><<<INFO
    // 后台系统描述
    'description' => '云静Admin TP5.1',
INFO
    ,
    "favicon"=><<<INFO
    // 后台站点图标
    'favicon' => '/favicon.ico',
INFO
    ,
    "middleware"=><<<INFO
    // 后台必要中间件
    "middleware"=>[],
INFO
    ,
    "welcome_route"=><<<INFO
    // 后台welcome路由地址
    // 格式详见：https://www.kancloud.cn/manual/thinkphp5_1/353966
    "welcome_route" => "\\app\\demo\\controller\\Index@welcome",
INFO
    ,
    "login_url"=><<<INFO
    // 后台登录页地址
    "login_url" => url("demo/login/index"),
INFO
    ,
    "parent_header_left_nav_file"=><<<INFO
    // 后台父页面header左边导航栏视图文件
    'parent_header_left_nav_file' => 'demo@public/parent/header_left_nav',
INFO
    ,
    "parent_header_right_nav_file"=><<<INFO
    // 后台父页面header右边导航栏视图文件
    'parent_header_right_nav_file' => 'demo@public/parent/header_right_nav',
INFO
    ,
    "parent_menu_file"=><<<INFO
    // 后台父页面menu视图文件路径
    'parent_menu_file' => 'demo@public/parent/menu',
INFO
    ,
    "iframe_breadcrumb_file"=><<<INFO
    // 后台子页面面包屑视图文件路径
    'iframe_breadcrumb_file' => 'demo@public/iframe/breadcrumb',
INFO
    ,
    "default_img"=><<<INFO
    // 默认图片地址
    "default_img" => request()->domain() . "/static/yunj/img/default.png",
INFO
    ,
    "theme"=><<<INFO
    /**
     * 自定义主题配置
     * 示例：
     *  'theme'=>[
     *      // 主题唯一code
     *      'default'=>[
     *          // 主题标题
     *          'title'=>'默认主题',
     *          // 主题模板样式文件路径
     *          'tpl_style_file'=>'/static/yunj/css/theme/default/tpl.css',
     *          // 主题样式文件路径/static/
     *          'style_file'=>'/static/yunj/css/theme/default/default.css',
     *      ],
     *  ],
     */
    'theme' => [
        'example' => [
            'title' => '示例主题',
            'tpl_style_file' => '/static/yunj/demo/css/theme/example/tpl.css?v=1.0.2',
            'style_file' => '/static/yunj/demo/css/theme/example/example.css?v=1.0.2',
        ],
    ],
INFO
    ,
];