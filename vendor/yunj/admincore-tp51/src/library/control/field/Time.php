<?php
namespace yunj\control\field;

class Time extends YunjField {

    private static $instance;

    public static function instance(){
        if (!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs():array{
        return [
            'placeholder' => '',                // 占位符
            'min'=>'',                          // 最小时间，格式HH:mm:ss
            'max'=>'',                          // 最大时间，格式HH:mm:ss
            'range'=>false,                    // 范围选择开启，bool类型或 range: '~' 来自定义分割字符，默认false，当为true时分隔符为'-'
            'disabled'=>false,                 // 禁用
        ];
    }

    protected function handleArgs($args): array {
        if (!strstr($args["verify"], "time"))
            $args["verify"] .= ($args["verify"] ? "|" : "") . "time";
        return $args;
    }

}