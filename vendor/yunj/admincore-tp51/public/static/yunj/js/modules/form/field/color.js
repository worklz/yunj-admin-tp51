/**
 * FormFieldColor
 */
layui.define(['FormField', "jquery"], function (exports) {

    let FormField = layui.FormField;
    let win = window;
    let doc = document;
    let $ = layui.jquery;

    class FormFieldColor extends FormField {

        constructor(options = {}) {
            super(options);
            this.color_action_box = null; // 颜色控制
            this.color_input_el = null;   // 颜色输入框
            this.color_show_el = null;    // 颜色句柄
        }

        defineExtraArgs() {
            let that = this;
            return {
                disabled: false
            };
        }

        handleArgs(args) {
            if (args.verify.indexOf("hexColor") === -1)
                args.verify += (args.verify ? "|" : "") + "hexColor";
            return args;
        }

        layoutControl() {
            let that = this;
            let controlHtml = `<div class="field-color-box">
                                    <div class="field-color-action-box" title="${that.args.disabled ? '禁用' : '选择/编辑颜色'}">
                                        <input type="text" class="layui-input field-color-action-input" readonly maxlength="7" autocomplete="off">
                                        <span class="field-color-action-show"></span>
                                    </div>
                                </div>`;
            return `<div class="layui-input-inline yunj-form-item-control">${controlHtml}</div>`;
        }

        async renderBefore() {
            let that = this;
            if (that.args.disabled) return 'done';
            // 修改了源码，增加了上下定位判断，增加了css
            await yunj.includeCss('/static/yunj/libs/colpick/colpick.css?v=1.0.6');
            await yunj.includeJs(`/static/yunj/libs/colpick/colpick.js?v=1.0.6`);
            return 'done';
        }

        renderDone() {
            let that = this;

            that.color_action_box = that.fieldBoxEl.find('.field-color-action-box');
            that.color_input_el = that.fieldBoxEl.find('.field-color-action-input');
            that.color_show_el = that.fieldBoxEl.find('.field-color-action-show');

            if (that.args.disabled) return false;
            that.color_action_box.colpick({
                layout: 'hex',
                submit: 0,
                colorScheme: 'dark',
                onChange: function (hsb, hex, rgb, el, bySetColor) {
                    that.setColor(`#${hex}`);
                }
            });
        }

        setValue(val = '') {
            let that = this;
            that.setColor(val);
            if (that.args.disabled) return false;
            that.color_action_box.colpickSetColor(val);
        }

        getValue() {
            let that = this;
            return that.color_input_el.val();
        }

        setColor(val) {
            let that = this;

            if (!yunj.isString(val) || !/^#[a-zA-Z0-9]{6}$/.test(val)) val = "#000000";
            that.color_input_el.val(val);
            that.color_show_el.css('backgroundColor', val);
        }

        defineExtraEventBind() {
            let that = this;

            // 防止重复绑定事件
            if (yunj.isUndefined(win.FORM_FIELD_COLOR_SCROLL_HIDE_COPY_EVENT_BIND)) {
                win.FORM_FIELD_COLOR_SCROLL_HIDE_COPY_EVENT_BIND = true;
                $(".yunj-iframe-content").scroll(function (e) {
                    let colpickEl = $(".colpick ");
                    if (colpickEl.length > 0) colpickEl.hide();
                });

                $(".layui-tab-content").scroll(function (e) {
                    let colpickEl = $(".colpick ");
                    if (colpickEl.length > 0) colpickEl.hide();
                });
            }
        }

    }

    exports('FormFieldColor', FormFieldColor);
});