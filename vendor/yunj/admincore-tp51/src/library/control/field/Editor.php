<?php

namespace yunj\control\field;

class Editor extends YunjField {

    private static $instance;

    public static function instance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs(): array {
        return [
            'mode' => 'ckeditor',         // 模式（可选值：ckeditor（默认））
            "modeConfig" => [             // 模式的配置
                "ckeditor" => [
                    "toolbar" => [
                        'Source', '-', 'Undo', 'Redo', '-', 'Preview', '-', 'SelectAll', '-', 'Bold', 'Italic', 'Underline', 'Strike', '-',
                        'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-',
                        'BidiLtr', 'BidiRtl', '-', 'Link', 'Image', 'CodeSnippet', '-', 'Styles', 'Format', 'Font', 'FontSize', '-',
                        'TextColor', 'BGColor', '-', 'Maximize'
                    ]
                ],
            ],
            'readonly' => false,        // 只读
        ];
    }

    protected function handleArgs($args): array {
        $mode = $args["mode"];
        $modeConfig = $args["modeConfig"];
        $defaultModeConfig = $this->defineExtraArgs()["modeConfig"];
        // 没有设置配置
        if (!isset($modeConfig[$mode])) {
            $modeConfig[$mode] = $defaultModeConfig[$mode];
            $args["modeConfig"] = $modeConfig;
            return $args;
        }
        // 有配置
        $modeConfig[$mode] += $defaultModeConfig[$mode];
        if ($mode === "ckeditor") {
            // 若果是ckeditor，对toolbar取交集
            $modeConfig[$mode]["toolbar"] = array_intersect($modeConfig[$mode]["toolbar"], $defaultModeConfig[$mode]["toolbar"]);
        }
        $args["modeConfig"] = $modeConfig;
        return $args;
    }

}