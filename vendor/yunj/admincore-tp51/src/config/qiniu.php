<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2020 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1732983738@qq.com>
// +----------------------------------------------------------------------
// | 七牛云配置
// +----------------------------------------------------------------------

return [
    // 是否开启，开启后检查配置和comporse是否有安装七牛云的SDK
    'is_enable'=>false,

    // AccessKey
    'access_key'=>'',

    // SecretKey
    'secret_key'=>'',

    // bucket存储空间
    'bucket'=>'',

    // 域名，示例：https://xxx.com
    'domain'=>'',
];