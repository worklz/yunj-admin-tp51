<?php
namespace yunj\control\field;

class Month extends YunjField {

    private static $instance;

    public static function instance(){
        if (!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs():array{
        return [
            'placeholder' => '',                // 占位符
            'min'=>'',                          // 最小月份，格式yyyy-MM
            'max'=>'',                          // 最大月份，格式yyyy-MM
            'range'=>false,                    // 范围选择开启，bool类型或 range: '~' 来自定义分割字符，默认false，当为true时分隔符为'-'
            'disabled'=>false,                 // 禁用
        ];
    }

    protected function handleArgs($args): array {
        if (!strstr($args["verify"], "month"))
            $args["verify"] .= ($args["verify"] ? "|" : "") . "month";
        return $args;
    }

}