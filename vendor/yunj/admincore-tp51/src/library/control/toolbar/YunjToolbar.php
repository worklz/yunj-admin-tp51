<?php
namespace yunj\control\toolbar;

class YunjToolbar {

    protected $requireArgs=[
        'type' => '',               // 触发事件类型（openTab 打开子页面、openPopup 打开弹出层页面、asyncEvent 异步事件）
        'title'=>'',                // 标题
        'class'=>'',                // 额外class（可选，可含图标class）
        'url'=>'',                  // 触发事件执行的url
        'confirmText'=>'',          // 确认弹窗提示（可选）
        'dropdown'=>false,          // 是否下拉操作
    ];

    // 工具栏操作项key
    protected $key;

    // 工具栏操作项配置
    protected $args;

    /**
     * Notes: 设置属性
     * Author: Uncle-L
     * Date: 2020/9/24
     * Time: 14:56
     * @param $key
     * @param $args
     * @return $this
     */
    public function setAttr($key,$args){
        $this->key=$key;
        $this->setArgs($args);
        return $this;
    }

    /**
     * Notes: 设置操作项配置
     * Author: Uncle-L
     * Date: 2020/8/10
     * Time: 12:14
     * @param $args
     */
    protected function setArgs($args){
        $args = array_supp($args,$this->requireArgs);
        $this->args=$args;
    }

    /**
     * Notes: 返回操作项配置
     * Author: Uncle-L
     * Date: 2020/8/10
     * Time: 12:13
     * @return mixed
     */
    public function args(){
        $this->args['event']=$this->key;
        return $this->args;
    }

}