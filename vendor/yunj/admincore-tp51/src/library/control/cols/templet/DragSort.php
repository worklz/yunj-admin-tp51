<?php

namespace yunj\control\cols\templet;

use yunj\control\cols\YunjCols;

class DragSort extends YunjCols {

    private static $instance;

    public static function instance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs(): array {
        return [
            'iconClass' => '',
        ];
    }

    protected function handleArgs(array $args): array {
        if (!$args["iconClass"]) $args["iconClass"] = "yunj-icon yunj-icon-sort-circle";
        return $args;
    }

}