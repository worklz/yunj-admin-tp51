<?php

namespace yunj;

use think\Model as ThinkModel;

class Model extends ThinkModel {

    /**
     * 新增时间戳字段
     * 当调用方法：addRow、addRows时会自动为新增时间戳字段附上当前时间戳
     * @var string
     */
    protected $createTimestampField = "create_time";

    /**
     * 修改时间戳字段
     * 当调用方法：addRow、addRows、change、changeBatch时会自动为修改时间戳字段附上当前时间戳
     * @var string
     */
    protected $updateTimestampField = "last_update_time";

    /*---------------------------------------------获取数据------------------------------------------------------------*/

    /**
     * Notes: 获取数据条数
     * Author: Uncle-L
     * Date: 2020/2/15
     * Time: 18:01
     * @param array $where
     * @return mixed
     */
    public function getCount($where = []) {
        $count = $this->where($where)->count();
        return $count;
    }

    /**
     * Notes: 获取本表数据条数
     * Author: Uncle-L
     * Date: 2020/2/15
     * Time: 18:08
     * @param array $where
     * @return mixed
     */
    public function getOwnCount($where = []) {
        $count = $this->where($where)->count();
        return $count;
    }

    /**
     * Notes: 获取本表某个字段的值
     * Author: Uncle-L
     * Date: 2020/2/15
     * Time: 18:09
     * @param array $where
     * @param string $field
     * @param array $order
     * @return mixed
     */
    public function getFieldValue($where = [], $field = '', $order = []) {
        $data = $this->where($where)->order($order)->value($field);
        return $data;
    }

    /**
     * Notes: 获取本表某个字段的所有值（一维数组）
     * Author: Uncle-L
     * Date: 2020/12/18
     * Time: 16:04
     * @param array $where
     * @param string $field
     * @param array $order
     * @return array
     */
    public function getColumnValue($where = [], $field = '', $order = []) {
        $data = $this->where($where)->order($order)->column($field);
        return $data;
    }

    /**
     * Notes: 获取单条数据
     * Author: Uncle-L
     * Date: 2020/7/20
     * Time: 16:35
     * @param array $where
     * @param array $field
     * @return array
     */
    public function getRow($where = [], $field = ["*"]) {
        $data = $this->where($where)->field($field)->find();
        return $data ? $data->toArray() : [];
    }

    /**
     * Notes: 获取本表单条数据
     * Author: Uncle-L
     * Date: 2020/7/20
     * Time: 16:36
     * @param array $where
     * @param array $field
     * @return array
     */
    public function getOwnRow($where = [], $field = ["*"]) {
        $data = $this->where($where)->field($field)->find();
        return $data ? $data->toArray() : [];
    }

    /**
     * Notes: 获取多条数据
     * Author: Uncle-L
     * Date: 2020/7/20
     * Time: 16:36
     * @param array $where
     * @param array $field
     * @param array $order
     * @param int $limitStart
     * @param int $limitLength
     * @return array
     */
    public function getRows($where = [], $field = ["*"], $order = ['id' => 'desc'], $limitStart = 0, $limitLength = 0) {
        $datas = $this->where($where)->field($field)->order($order);
        if ($limitLength) $datas = $datas->limit($limitStart, $limitLength);
        $datas = $datas->select();
        return $datas ? $datas->toArray() : [];
    }

    /**
     * Notes: 获取本表多条数据
     * Author: Uncle-L
     * Date: 2020/7/20
     * Time: 16:36
     * @param array $where
     * @param array $field
     * @param array $order
     * @param int $limitStart
     * @param int $limitLength
     * @return array
     */
    public function getOwnRows($where = [], $field = ["*"], $order = ['id' => 'desc'], $limitStart = 0, $limitLength = 0) {
        $datas = $this->where($where)->field($field)->order($order);
        if ($limitLength) $datas->limit($limitStart, $limitLength);
        $datas = $datas->select();
        return $datas ? $datas->toArray() : [];
    }

    /*---------------------------------------------操作数据------------------------------------------------------------*/

    /**
     * Notes: 执行操作之后执行
     * Author: Uncle-L
     * Date: 2020/2/16
     * Time: 11:33
     */
    protected function executeAfter() {
    }

    /**
     * Notes: 添加单条数据，若主键为自增id，则返回结果id；反之返回影响行数
     * Author: Uncle-L
     * Date: 2020/2/15
     * Time: 21:10
     * @param $data
     * @param bool $incId [添加数据是否自增id]
     * @return int|string
     */
    public function addRow($data, $incId = true) {
        if ($this->createTimestampField || $this->updateTimestampField) {
            $currTimestamp = time();
            $createTimestampField = $this->createTimestampField;
            if ($createTimestampField)
                $data[$createTimestampField] = isset($data[$createTimestampField]) ? $data[$createTimestampField] : $currTimestamp;
            $updateTimestampField = $this->updateTimestampField;
            if ($updateTimestampField)
                $data[$updateTimestampField] = isset($data[$updateTimestampField]) ? $data[$updateTimestampField] : $currTimestamp;
        }
        $rawData = $this->allowField(true)->strict(false);
        $res = $incId ? $rawData->insertGetId($data) : $rawData->insert($data);
        if ($res) {
            $this->addRowAfter();
            $this->executeAfter();
        }
        return $res;
    }

    /**
     * Notes: 添加一条数据后执行
     * Author: Uncle-L
     * Date: 2020/2/15
     * Time: 21:10
     */
    protected function addRowAfter() {
    }

    /**
     * Notes: 添加多条数据，返回添加成功的条数
     * Author: Uncle-L
     * Date: 2020/2/15
     * Time: 21:11
     * @param $data
     * @return int|string
     */
    public function addRows($data) {
        if ($this->createTimestampField || $this->updateTimestampField) {
            $currTimestamp = time();
            $createTimestampField = $this->createTimestampField;
            $updateTimestampField = $this->updateTimestampField;
            foreach ($data as &$v) {
                if ($createTimestampField) $v[$createTimestampField] = isset($v[$createTimestampField]) ? $v[$createTimestampField] : $currTimestamp;
                if ($updateTimestampField) $v[$updateTimestampField] = isset($v[$updateTimestampField]) ? $v[$updateTimestampField] : $currTimestamp;
            }
        }
        $res = $this->allowField(true)->strict(false)->limit(100)->insertAll($data);
        if ($res) {
            $this->addRowsAfter();
            $this->executeAfter();
        }
        return $res;
    }

    /**
     * Notes: 添加多条数据后执行
     * Author: Uncle-L
     * Date: 2020/2/15
     * Time: 21:11
     */
    protected function addRowsAfter() {
    }

    /**
     * Notes: 更新数据，返回影响的记录数
     * Author: Uncle-L
     * Date: 2020/7/20
     * Time: 16:37
     * @param $data
     * @param array $where
     * @return int|string
     */
    public function change($data, $where = []) {
        if ($this->updateTimestampField) {
            $currTimestamp = time();
            $updateTimestampField = $this->updateTimestampField;
            $data[$updateTimestampField] = isset($data[$updateTimestampField]) ? $data[$updateTimestampField] : time();
        }
        $res = $this->allowField(true)->strict(false)->where($where)->update($data);
        if ($res) {
            $this->changeAfter();
            $this->executeAfter();
        }
        return $res;
    }

    /**
     * Notes: 更新数据后执行
     * Author: Uncle-L
     * Date: 2020/2/15
     * Time: 21:15
     */
    protected function changeAfter() {
    }

    /**
     * Notes: 批量更新数据，在批量更新的数据中包含主键,返回一个数据集对象
     * Author: Uncle-L
     * Date: 2020/2/15
     * Time: 21:22
     * @param $data
     * @return \think\Collection
     */
    public function changeBatch($data) {
        if ($this->updateTimestampField) {
            $currTimestamp = time();
            $updateTimestampField = $this->updateTimestampField;
            foreach ($data as &$v)
                $v[$updateTimestampField] = isset($v[$updateTimestampField]) ? $v[$updateTimestampField] : $currTimestamp;
        }
        $res = $this->allowField(true)->saveAll($data);
        if ($res) {
            $this->changeBatchAfter();
            $this->executeAfter();
        }
        return $res;
    }

    /**
     * Notes: 批量更新数据后执行
     * Author: Uncle-L
     * Date: 2020/2/15
     * Time: 21:22
     */
    protected function changeBatchAfter() {
    }

    /**
     * Notes: 字段自增，返回影响数据的条数
     * Author: Uncle-L
     * Date: 2020/7/20
     * Time: 16:38
     * @param string $field
     * @param array $where
     * @param int $step
     * @return int|true
     */
    public function fieldInc($field = '', $where = [], $step = 1) {
        $res = $this->where($where)->setInc($field, $step);
        return $res;
    }

    /**
     * Notes: 字段自减，返回影响数据的条数
     * Author: Uncle-L
     * Date: 2020/7/20
     * Time: 16:38
     * @param string $field
     * @param array $where
     * @param int $step
     * @return int|true
     */
    public function fieldDec($field = '', $where = [], $step = 1) {
        $res = $this->where($where)->setDec($field, $step);
        return $res;
    }

    /**
     * Notes: 物理删除，返回影响的记录数
     * Author: Uncle-L
     * Date: 2021/11/30
     * Time: 18:29
     * @param $where
     * @return bool
     */
    public function del($where) {
        $res = $this->where($where)->delete();
        if ($res) {
            $this->delAfter();
            $this->executeAfter();
        }
        return $res;
    }

    /**
     * Notes: 物理删除后执行
     * Author: Uncle-L
     * Date: 2020/2/16
     * Time: 11:36
     */
    protected function delAfter() {
    }
}