<?php

namespace yunj\control\field;

class Area extends YunjField {

    private static $instance;

    public static function instance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs(): array {
        return [
            'acc' => 'district',     // 精度（可选值：province、city、district默认）
            'disabled' => false       // 禁用
        ];
    }

    protected function handleArgs($args): array {
        if (!strstr($args["verify"], "area"))
            $args["verify"] .= ($args["verify"] ? "|" : "") . "area:" . $args["acc"];
        return $args;
    }

}