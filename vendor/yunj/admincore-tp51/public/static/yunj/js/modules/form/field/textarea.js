/**
 * FormFieldTextarea
 */
layui.define(['FormField'], function (exports) {

    let FormField = layui.FormField;

    class FormFieldTextarea extends FormField {

        constructor(options={}) {
            super(options);
        }

        defineExtraArgs(){
            let that = this;
            return {
                placeholder:"",
                readonly:false
            };
        }

        defineBoxHtml(){
            let that = this;
            return `<div class="layui-form-item yunj-form-item layui-form-textarea" id="${that.id}">__layout__</div>`;
        }

        layoutControl() {
            let that = this;
            let controlHtml = `<textarea name="${that.id}" ${that.args.readonly ? 'readonly' : ''}
                        placeholder="${that.args.placeholder}" class="layui-textarea"></textarea>`;
            return `<div class="layui-input-inline yunj-form-item-control">${controlHtml}</div>`;
        }

        setValue(val=''){
            let that=this;
            that.fieldBoxEl.find(`textarea[name=${that.id}]`).val(val);
        }

        getValue(){
            let that=this;
            return that.fieldBoxEl.find(`textarea[name=${that.id}]`).val();
        }

    }

    exports('FormFieldTextarea', FormFieldTextarea);
});