<?php

namespace app\demo\enum;

final class Hobby extends Enum {

    const WRITE = "write";

    const READ = "read";

    public static function getTitleMap(): array {
        return [
            self::WRITE => "写作",
            self::READ => "阅读",
        ];
    }

}