# 云静Admin核心 TP5.1

[![](https://img.shields.io/badge/Author-Uncle.L-orange.svg)](https://gitee.com/worklz/yunj-admincore-tp51)
[![](https://img.shields.io/badge/version-v1.06.701-brightgreen.svg)](https://gitee.com/worklz/yunj-admincore-tp51)
[![star](https://gitee.com/worklz/yunj-admincore-tp51/badge/star.svg?theme=dark)](https://gitee.com/worklz/yunj-admincore-tp51/stargazers)
[![fork](https://gitee.com/worklz/yunj-admincore-tp51/badge/fork.svg?theme=dark)](https://gitee.com/worklz/yunj-admincore-tp51)

## 简介

**云静Admin核心 TP5.1**是云静Admin TP5.1的核心代码包

## 安装

> 前提

* 安装了TP5.1项目
* 创建以下文件夹，并赋予写入权限
    * 根目录\application\demo
    * 根目录\yunj
    * 根目录\public\static\yunj

> 安装composer包

注意：最近phpcomposer镜像存在问题，可以改成：`composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/`

```
composer require yunj/admincore-tp51
```

> 初始化配置

`根目录\application\tag.php`配置如下内容
```php
// 应用行为扩展定义文件
return [
    // 应用初始化
    'app_init'     => [
        "\yunj\Init",
    ],...
];
```

> 浏览器中访问进行初始化

```
http://xxx.com/admin
```
   
> 版本更新

```
composer update yunj/admincore-tp51
```

## 演示截图

PC端截图
![](https://img.kancloud.cn/35/92/3592ab383875e61ed03df803a5d8bdb8_1920x902.png)
移动端截图
![](https://img.kancloud.cn/f7/af/f7af24b631219679d6474bd373ac95f8_1920x811.png)

## 演示地址

* 地址一：[http://tp51admin.iyunj.cn/admin](http://tp51admin.iyunj.cn/admin)
* 地址二：[http://tp51admin.lzadmin.top/admin](http://tp51admin.lzadmin.top/admin)

## 文档

[https://www.kancloud.cn/worklz/yunj_admin_tp51/2526381](https://www.kancloud.cn/worklz/yunj_admin_tp51/2526381)

## QQ交流群

* `1154038256`

## 参与开发

直接提交PR或者Issue即可