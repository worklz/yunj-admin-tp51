/**
 * 云静表格
 */
layui.use(['jquery', 'yunj', 'elemProgress'], function () {
    let win = window;
    let doc = document;
    let $ = layui.jquery;
    let elemProgress = layui.elemProgress;

    class YunjTable {

        constructor(obj) {
            this.id = obj.id;                         // 当前对象id

            this.rawEl = obj.elem;                    // 原始元素

            this.rawArgs = obj.args;                  // 原始数据

            this.boxEl = null;                       // 顶部父元素

            this.url = null;                         // 请求地址

            this.buildNames = [];                     // 当前构件实例对象名数组，控制构件加载顺序

            this.buildMap = {};                       // 当前构件实例对象map

            this._init().catch(e => {
                yunj.error(e)
            });
        }

        async _init() {
            let that = this;
            if (!that.id || !that.rawArgs || yunj.isEmptyObj(that.rawArgs)) throw new Error("表格构建器参数异常");
            // 初始化数据
            await that._initData();
            // 进度0
            let elemProgressObj = elemProgress({elem: that.boxEl});
            // 渲染
            that.render().then(res => {
                // 设置事件绑定
                that.setEventBind();
                // 进度100%
                elemProgressObj.reset_progress(100);
            });
        }

        /**
         * 原始数据是否设置某个属性
         * @param {string} attr
         * @private {bool}
         */
        rawArgsIsSetAttr(attr) {
            return this.rawArgs.hasOwnProperty(attr);
        }

        // 判断是否设置state
        isSetState() {
            return this.rawArgsIsSetAttr("state");
        }

        // 判断是否设置url
        isSetUrl() {
            return this.rawArgsIsSetAttr("url");
        }

        // 判断是否设置page
        isSetPage() {
            return this.rawArgsIsSetAttr("page");
        }

        // 判断是否设置limit
        isSetLimit() {
            return this.rawArgsIsSetAttr("limit");
        }

        // 判断是否设置limits
        isSetLimits() {
            return this.rawArgsIsSetAttr("limits");
        }

        // 判断是否设置filter
        isSetFilter() {
            return this.rawArgsIsSetAttr("filter");
        }

        // 判断是否设置toolbar
        isSetToolbar() {
            return this.rawArgsIsSetAttr("toolbar");
        }

        // 判断是否设置filter
        isSetDefaultToolbar() {
            return this.rawArgsIsSetAttr("defaultToolbar");
        }

        // 判断是否设置import
        isSetImport() {
            return this.rawArgsIsSetAttr("import");
        }

        // 判断是否设置cols
        isSetCols() {
            return this.rawArgsIsSetAttr("cols");
        }

        // 数据初始化
        async _initData() {
            let that = this;
            // boxEl
            that.rawEl.after(`<div class="yunj-table-box" lay-filter="${that.id}"></div>`);
            that.boxEl = $(`.yunj-table-box[lay-filter=${that.id}]`);
            // url
            that.url = that.isSetUrl() ? that.rawArgs.url : yunj.url(false);

            // build
            let buildArr = ["state", "filter", "layTable"];
            for (let i = 0, l = buildArr.length; i < l; i++) {
                let buildUcfirst = buildArr[i].slice(0, 1).toUpperCase() + buildArr[i].slice(1);
                if (!that[`isSet${buildArr[i]==="layTable"?"Cols":buildUcfirst}`]()) continue;
                let layModule = `TableBuild${buildUcfirst}`;
                let build = await new Promise(resolve => {
                    layui.use(layModule, () => {
                        resolve(new layui[layModule](that));
                    });
                });
                that.buildNames.push(build.buildName);
                that.buildMap[build.buildName] = build;
            }
        }

        // 渲染
        async render() {
            let that = this;

            for(let i=0,l=that.buildNames.length;i<l;i++){
                await that.buildMap[that.buildNames[i]].render();
            }

            // 渲染完成事件触发
            $(doc).trigger(`yunj_table_${that.id}_render_done`);
        }

        // 设置事件绑定
        setEventBind() {
            let that = this;

            for(let i=0,l=that.buildNames.length;i<l;i++){
                that.buildMap[that.buildNames[i]].setEventBind();
            }

            // 文档页面宽度发生变化时触发
            $(doc).resize(function () {
                $(doc).trigger(`yunj_table_${that.id}_doc_width_change`);
            });
        }

        // 获取当前状态值
        getCurrState() {
            return this.isSetState() ? this.buildMap.state.getValue() : undefined;
        }

        /**
         * 获取当前请求筛选条件
         * @param args
         * @returns {any}
         */
        getCurrRequestFilter(args = {}) {
            let that = this;
            args = yunj.objSupp(args, {
                filter: true,                   // 是否包含筛选表单的值
                ids: false,                     // 是否包含当前选中ids
                convert: true,                  // 是否转换为json字符串
                state: that.getCurrState(),     // 指定状态下的筛选条件（默认当前状态）
            });
            let data = {}
            // 触发获取请求filter data的事件
            $(doc).trigger(`yunj_table_${that.id}_get_request_filter_data`, [data, args]);
            return args.convert ? JSON.stringify(data) : data;
        }
    }

    $(doc).ready(function () {
        win.yunj.table = {};

        let tableEls = $('table[type=yunj]');
        if (yunj.isUndefined(YUNJ_TABLE) || !yunj.isObj(YUNJ_TABLE) || tableEls.length <= 0) return;
        tableEls.each(function () {
            let id = $(this).attr('id');
            if (!YUNJ_TABLE.hasOwnProperty(id)) return true;
            let args = YUNJ_TABLE[id];
            win.yunj.table[id] = new YunjTable({
                id: id,
                elem: $(this),
                args: args
            });
        });
    });

});
