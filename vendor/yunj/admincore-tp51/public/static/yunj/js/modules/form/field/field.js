/**
 * FormField
 */
layui.define(['jquery', 'yunj'], function (exports) {
    let win = window;
    let doc = document;
    let $ = layui.jquery;

    class FormField {

        constructor(options = {}) {
            this.options = options;
            this.formId = options.formId;               // 表单id
            this.tab = options.tab;                     // 当前表单tab
            this.key = options.key;                     // 字段key

            this.id = null;                         // id
            this.args = {};                         // 字段配置数据
            this.tabFormFilter = null;              // tab栏的表单标识
            this.boxHtml = null;                    // 字段盒子html

            this.fieldBoxEl = null;                 // 字段

            this._init();
        }

        _init() {
            let that = this;
            // 设置数据
            that._setData();
        }

        // 设置数据
        _setData() {
            let that = this;
            that._setId();
            that._setArgs();
            that._setTabFormFilter();
            that._setBoxHtml();
        }

        // 生成id
        _setId() {
            let that = this;
            let id = that.formId;
            if (that.tab.length > 0) id += `_${that.tab}`;
            id += `_${that.key}`;
            that.id = id;
        }

        // 设置参数
        _setArgs() {
            let that = this;
            let args = Object.assign({
                title: "",
                default: "",
                desc: "",
                verify: "",
                verifyTitle: ""
            }, that.defineExtraArgs(), that.options.args);
            args = that.handleArgs(args);
            // 必要参数
            that.args = args;
        }

        // 定义额外的args参数
        defineExtraArgs() {
            return {};
        }

        /**
         * 处理args
         * @param {object} args
         * @return {object}
         */
        handleArgs(args) {
            return args;
        }

        // 设置tabFormFilter 用于某些字段的渲染 form.render
        _setTabFormFilter() {
            let that = this;
            let tabFormFilter = that.formId + "_form" + (that.tab ? `_${that.tab}` : "");
            let tabFormBoxEl = $(`[lay-filter=${tabFormFilter}]`);
            if (tabFormBoxEl.length <= 0) {
                let error = `lay-filter=${tabFormFilter}的表单容器不存在`;
                yunj.alert(error);
                throw new Error(error);
            }
            if (!tabFormBoxEl.hasClass("layui-form")) tabFormBoxEl.addClass("layui-form");
            that.tabFormFilter = tabFormFilter;
        }

        // 设置布局
        _setBoxHtml() {
            let that = this;
            that.boxHtml = that.defineBoxHtml();
        }

        // 定义字段外部包裹元素html结构，id和__layout__必须
        defineBoxHtml() {
            let that = this;
            return `<div class="layui-form-item yunj-form-item" id="${that.id}">__layout__</div>`;
        }

        // 获取初始化值
        _getInitValue() {
            let that = this;
            return that.args.hasOwnProperty("value") ? that.args.value : that.args.default;
        }

        /**
         * 渲染
         * @param {string} parentSelector    [字段整体结构所在的外部父容器的jquery选择器]
         * @param {string} fieldOutLayout    [字段整体结构外部附加结构]
         * 当传入外部附加结构时，如：<div>__layout__</div>，会用生成的结构替换掉关键词 __layout__
         * @return {FormField}
         */
        async render(parentSelector, fieldOutLayout = "") {
            let that = this;
            await that.renderBefore();
            let layout = that._layout();
            if (fieldOutLayout && fieldOutLayout.indexOf('__layout__') !== -1) layout = fieldOutLayout.replace('__layout__', layout);
            $(parentSelector).append(layout);
            // 表单盒子元素
            that.fieldBoxEl = $(`#${that.id}`);
            // 渲染后执行
            await that.renderDone();
            // 渲染完后设置值
            that.renderDoneSetValue(that._getInitValue());
            // 设置字段事件绑定
            that._setEventBind();

            return that;
        }

        // 字段布局
        _layout() {
            let that = this;
            let label = that.layoutLabel();
            let control = that.layoutControl();
            let desc = that.layoutDesc();
            return that.boxHtml.replace('__layout__', label + control + desc);
        }

        // 返回字段label的html结构
        layoutLabel() {
            let that = this;
            let labelHtml = '';
            if (!that.args.title) return labelHtml;
            if (that.args.verify && that.args.verify.indexOf('require') !== -1) labelHtml += `<span class="require">*</span>`;
            labelHtml += that.args.title;
            return `<label class="layui-form-label">${labelHtml}</label>`;
        }

        // 返回字段控件的html结构
        layoutControl() {
        }

        // 返回字段简介的html结构
        layoutDesc() {
            let that = this;
            if (!that.args.desc) return "";
            return `<div class="yunj-form-item-desc">${that.args.desc}</div>`;
        }

        // 渲染前执行
        async renderBefore() {
            return 'done';
        }

        // 渲染后执行
        async renderDone() {
            return 'done';
        }

        // 渲染完后设置值
        // setValue不能放在可能不存在的事件里执行，防止单一字段调用时不生效
        renderDoneSetValue(val) {
            let that = this;
            that.setValue(val);
        };

        // 设置值
        setValue(val = "") {
        }

        // 获取值
        getValue() {
        }

        // 设置事件绑定
        _setEventBind() {
            let that = this;

            // 清空
            $(doc).bind(`yunj_form_${that.formId}_clear`, function (e) {
                that.setValue("");
            });

            // 重置
            $(doc).bind(`yunj_form_${that.formId}_reset`, function (e) {
                that.setValue(that._getInitValue());
            });

            // 提交
            $(doc).bind(`yunj_form_${that.formId}_submit`, function (e, data, verifyArgs) {
                let key = that.key;
                let val = that.getValue();
                data[key] = val;
                if (!verifyArgs.enable || !that.args.verify) return;
                verifyArgs.rule[key] = that.args.verify;
                verifyArgs.data[key] = val;
                verifyArgs.dataTitle[key] = that.args.verifyTitle || that.args.title || key;
            });

            that.defineExtraEventBind();
        }

        // 定义额外的事件绑定
        defineExtraEventBind() {
        }

    }

    exports('FormField', FormField);
});