<?php

namespace app\demo\controller\view_demo;

use app\demo\controller\Controller;

class BlockRewrite extends Controller {

    public function seo() {
        return $this->fetch();
    }

    public function headStyle() {
        return $this->fetch();
    }

    public function headScript() {
        return $this->fetch();
    }

    public function content() {
        return $this->fetch();
    }

    public function script() {
        return $this->fetch();
    }

}