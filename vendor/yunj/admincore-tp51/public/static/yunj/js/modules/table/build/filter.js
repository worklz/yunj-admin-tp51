/**
 * TableBuildFilter
 */
layui.define(['jquery', 'yunj',"TableBuild",'button','validate'], function (exports) {

    let win = window;
    let doc = document;
    let $ = layui.jquery;
    let TableBuild = layui.TableBuild;
    let button = layui.button;
    let validate = layui.validate;

    class TableBuildFilter extends TableBuild{

        constructor(table) {
            super(table,"filter");
            this.fieldObjs = {};    // 筛选表单字段实例容器
            this.currFormEl = null; // 当前表单元素
        }

        // 渲染
        async render() {
            let that = this;

            that._renderBefore();

            await that._renderFields();

            let formEl = that.buildBoxEl.find('.filter-form');
            if (formEl.length <= 0) return;
            formEl.removeClass('filter-form-this');

            let currFormEl = that.buildBoxEl.find(`.filter-form[lay-filter=${that.getCurrLayFilter()}]`);
            if (currFormEl.length <= 0) return;
            currFormEl.addClass('filter-form-this');
            that.currFormEl = currFormEl;

            // 设置当前表单布局
            that._setCurrFormLayout();

            that._renderAfter();
        }

        // 渲染字段
        async _renderFields() {
            let that = this;
            if (!yunj.isEmptyObj(that.fieldObjs)) return;

            let filter = that.buildArgs;
            if (that.isSetState()) {
                for (let state in filter) {
                    if (!filter.hasOwnProperty(state)) continue;
                    let formId = that.generateFormId(state);
                    let formLayFilter = that.generateLayFilter(formId);
                    let filterFields = filter[state];
                    that.buildBoxEl.append(`<form class="layui-row layui-form layui-form-pane yunj-form filter-form" lay-filter="${formLayFilter}">`);
                    await that._renderFormFields(formId, filter[state]);
                }
            } else {
                let formId = that.generateFormId();
                that.buildBoxEl.append(`<form class="layui-row layui-form layui-form-pane yunj-form filter-form" lay-filter="${that.generateLayFilter(formId)}">`);
                await that._renderFormFields(formId, filter);
            }
        }

        // 渲染指定表单字段
        async _renderFormFields(formId,fields){
            let that = this;
            let layFilter = that.generateLayFilter(formId);
            for (let key in fields) {
                if (!fields.hasOwnProperty(key)) continue;
                let args = fields[key];
                await new Promise((resolve) => {
                    yunj.formField(args.type, {
                        formId: formId,
                        key: key,
                        args: args,
                    }).then(field => {
                        field.render(
                            `.filter-form[lay-filter=${layFilter}]`,
                            `<div class="layui-col-xs12 layui-col-sm6 layui-col-md3 filter-form-item-box">__layout__</div>`
                        ).then(res => {
                            that.fieldObjs[field.id] = field;
                            resolve();
                        });
                    });
                });
            }
            that.buildBoxEl.find(`.filter-form[lay-filter=${layFilter}]`).append(`<div class="layui-col-xs12 layui-col-sm6 layui-col-md3 filter-form-item-box"><div class="layui-form-item yunj-form-item"></div></div>`);
        }

        // 设置当前表单布局
        _setCurrFormLayout(){
            let that = this;
            if (!that.currFormEl) return;
            let currFormEl = that.currFormEl;

            that._setCurrFormLabelLayout();

            that._setCurrFormButtonLayout();

            //判断筛选字段数量是否>2（包含按钮）
            if (currFormEl.find(".filter-form-item-box").length <= 2) return;

            //适配屏宽
            let otherFormItemEl = currFormEl.find(".filter-form-item-box:not(:first,:last)");
            if (win.screen.width > 768) {
                //展开其他的filter-form-item-box
                otherFormItemEl.show('fast');
                //隐藏展开、收起按钮
                currFormEl.removeClass('filter-form-stow').addClass('filter-form-unfold');
            } else {
                //隐藏其他的filter-form-item-box
                otherFormItemEl.hide('fast');
                //显示展开、收起按钮
                currFormEl.removeClass('filter-form-stow').addClass('filter-form-unfold');
            }
        }

        // 设置当前表单字段label布局
        _setCurrFormLabelLayout() {
            let that = this;
            let labelMaxWidth = 0;
            let formItemEl = that.currFormEl.find('.yunj-form-item:not(.layui-form-textarea):not(.yunj-form-editor):not(.yunj-form-markdown)');
            formItemEl.each(function () {
                let currLabelWidth = $(this).find('.layui-form-label').outerWidth();
                if (currLabelWidth > labelMaxWidth) labelMaxWidth = currLabelWidth;
            });
            formItemEl.find('.layui-form-label').css('width', labelMaxWidth + 2 + 'px');
        }

        // 设置筛选表单按钮布局
        _setCurrFormButtonLayout() {
            let that = this;
            let lastFormItemEl = that.currFormEl.find('.yunj-form-item:last');
            if (lastFormItemEl.html().length > 0) return;
            let layFilter = that.getCurrLayFilter();
            let boxFilter = `.filter-form[lay-filter=${layFilter}] .yunj-form-item:last`;
            button.search(layFilter).render(boxFilter);
            button.reset(layFilter).render(boxFilter);
            lastFormItemEl.append(`<button type="button" class="layui-btn layui-btn-sm layui-btn-primary layui-icon filter-form-layout-control"></button>`);
        }

        // 渲染前
        _renderBefore(){
        }

        // 渲染后
        _renderAfter(){
            let that = this;

            // 绑定获取请求filter data的触发事件
            let eventRepeatKey = `YUNJ_TABLE_${that.tableId}_GET_REQUEST_FILTER_DATA_EVENT_BIND_BY_FILTER`;
            if (yunj.isUndefined(win[eventRepeatKey])) {
                win[eventRepeatKey] = true;
                $(doc).bind(`yunj_table_${that.tableId}_get_request_filter_data`, function (e,data,args) {
                    if(!args.filter) return;
                    let filterValues = yunj.formData(that.generateFormId(args.state?args.state:false), validate);
                    Object.assign(data, filterValues);
                });
            }
        }

        setEventBind(){
            let that = this;

            // 绑定文档宽度发生变化时触发
            $(doc).bind(`yunj_table_${that.tableId}_doc_width_change`, function (e) {
                that._setCurrFormLayout();
            });
            // unfold展开/stow收起
            that.buildBoxEl.on('click', '.filter-form-layout-control', function () {
                // 是否展开
                let isUnfold = that.currFormEl.hasClass('filter-form-unfold');
                that.currFormEl.removeClass(`filter-form-${isUnfold?"unfold":"stow"}`).addClass(`filter-form-${isUnfold?"stow":"unfold"}`);
                that.currFormEl.find(".filter-form-item-box:not(:first,:last)").slideToggle("fast");
            });
            // 重置
            that.buildBoxEl.on('click', '.yunj-btn-reset', function () {
                $(doc).trigger(`yunj_form_${that.getCurrFormId()}_clear`);
            });
            // 搜索
            that.buildBoxEl.on('click', '.yunj-btn-search', function () {
                // 筛选表单提交的触发事件
                $(doc).trigger(`yunj_table_${that.tableId}_filter_submit`);
            });
        }

        // 生成筛选表单对象id
        generateFormId(state = false) {
            return this.tableId + (state !== false ? `_${state}` : "");
        }

        // 生成筛选表单lay-filter，根据传入formId
        generateLayFilter(formId) {
            return formId + "_form";
        }

        // 获取当前筛选表单对象id
        getCurrFormId() {
            return this.tableId + (this.isSetState() ? `_${this.getCurrState()}` : "");
        }

        // 获取当前筛选表单lay-filter
        getCurrLayFilter() {
            return this.generateLayFilter(this.getCurrFormId());
        }

    }

    exports('TableBuildFilter', TableBuildFilter);
});