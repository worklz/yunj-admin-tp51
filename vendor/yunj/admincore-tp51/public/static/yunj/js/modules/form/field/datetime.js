/**
 * FormFieldDatetime
 */
layui.define(['FormField', 'laydate'], function (exports) {

    let FormField = layui.FormField;
    let laydate = layui.laydate;

    class FormFieldDatetime extends FormField {

        constructor(options = {}) {
            super(options);
        }

        defineExtraArgs() {
            let that = this;
            return {
                placeholder: "",
                min: "",
                max: "",
                range: false,
                disabled: false
            };
        }

        handleArgs(args) {
            if (args.verify.indexOf("datetime") === -1)
                args.verify += (args.verify ? "|" : "") + "datetime";
            return args;
        }

        layoutControl() {
            let that = this;
            let controlHtml = `<input type="text" name="${that.id}" placeholder="${that.args.placeholder}" 
                                    class="layui-input" autocomplete="off" readonly style="cursor:pointer;">`;
            return `<div class="layui-input-inline yunj-form-item-control">${controlHtml}</div>`;
        }

        setValue(val = '') {
            let that = this;
            if (!yunj.isScalar(val)) val = "";
            if (yunj.isTimestamp(val)) val = yunj.timestampFormat(val);
            that.fieldBoxEl.find(`input:text[name=${that.id}]`).val(val);
        }

        getValue() {
            let that = this;
            return that.fieldBoxEl.find(`input:text[name=${that.id}]`).val();
        }

        renderDone() {
            let that = this;
            if (that.args.disabled) return;
            let args = {
                elem: `input[name=${that.id}]`,
                type: 'datetime',
                format: "yyyy-MM-dd HH:mm:ss",
                range: (yunj.isString(that.args.range) || yunj.isBool(that.args.range) ? that.args.range : false),
                trigger: 'click',
            };
            if (that.args.min) args.min = that.args.min;
            if (that.args.max) args.min = that.args.max;
            laydate.render(args);
        }

    }

    exports('FormFieldDatetime', FormFieldDatetime);
});