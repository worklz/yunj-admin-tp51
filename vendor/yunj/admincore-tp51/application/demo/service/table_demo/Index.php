<?php

namespace app\demo\service\table_demo;

use app\demo\enum\State;

final class Index {

    private static $instance;

    public static function getInstance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Notes: 状态栏
     * Author: Uncle-L
     * Date: 2021/11/14
     * Time: 13:33
     * @return array
     */
    public function state(): array {
        return [1 => "状态一", 2 => "状态二"];
    }

    /**
     * Notes: 表头
     * Author: Uncle-L
     * Date: 2021/11/14
     * Time: 13:42
     * @param $state [状态]
     * @param bool $isDragSort [是否拖拽排序]
     * @return array
     */
    public function cols($state, $isDragSort = false): array {
        $cols = [
            'text1' => ['title' => '文本1'],
            'enum' => [
                'title' => '枚举',
                "templet" => "enum",
                "options" => [
                    1 => "一年级",
                    2 => "二年级",
                    3 => "三年级",
                    4 => "四年级",
                ]
            ],
            'datetime' => ['title' => '时间日期', "templet" => "datetime"],
            'img' => ['title' => '单图', "templet" => "img"],
            'imgs' => ['title' => '多图', "templet" => "imgs"],
            'file' => ['title' => '单文件', "templet" => "file"],
            'files' => ['title' => '多文件', "templet" => "files"],
            'color' => ['title' => '颜色', "templet" => "color"],
            'action' => [
                'title' => '操作',
                'templet' => 'action',
                'options' => [
                    'edit' => ['type' => 'openPopup', 'title' => '详情', 'class' => 'layui-icon layui-icon-survey', 'url' => url('edit')],
                    State::NORMAL => ['title' => '还原', 'dropdown' => true],
                    State::RECYLE_BIN => ['title' => '移入回收站', 'dropdown' => true],
                    State::DELETED => ['title' => '永久删除', 'dropdown' => true],
                ]
            ]
        ];
        if ($isDragSort){
            $cols["text1"]["templet"] = "dragSort";
            array_insert($cols,[
                "text2"=>[
                    'title' => '文本2',
                    'templet' => 'dragSort'
                ],
                "dragSortAction"=>[
                    'title' => '排序',
                    'align' => 'center',
                    'templet' => 'dragSort'
                ],
            ],"enum");
        }
        return $cols;
    }

    /**
     * Notes: 数量
     * Author: Uncle-L
     * Date: 2021/11/14
     * Time: 13:50
     * @param $filter [筛选条件]
     * @return int
     */
    public function count($filter): int {
        // 固定参数
        $state = $filter['state'];
        $ids = $filter['ids'];

        // 业务处理：根据当前筛选条件获取数据量

        return 5;
    }

    /**
     * Notes: 数量
     * Author: Uncle-L
     * Date: 2021/11/14
     * Time: 13:50
     * @param $filter [筛选条件]
     * @return array
     */
    public function items($limitStart, $limitLength, $filter, $sort): array {
        // 固定参数
        $state = $filter['state'];
        $ids = $filter['ids'];

        // 业务处理：根据当前筛选条件获取数据量

        $items = [
            [
                "id" => 1,
                "text1" => "测试内容1-1",
                "text2" => "测试内容2-1",
                "enum" => 1,
                "datetime" => "1636869353",
                "img" => "/static/yunj/img/default.png",
                "imgs" => ["/static/yunj/img/default.png", "/static/yunj/img/default.png"],
                "file" => ["name" => "default.png", "url" => "/static/yunj/img/default.png"],
                "files" => [["name" => "default.png", "url" => "/static/yunj/img/default.png"], ["name" => "bg.png", "url" => "/static/yunj/img/bg.png"]],
                "color" => "#fff000",
            ],
            [
                "id" => 2,
                "text1" => "测试内容1-2",
                "text2" => "测试内容2-2",
                "enum" => 1,
                "datetime" => "1636869353",
                "img" => "/static/yunj/img/default.png",
                "imgs" => ["/static/yunj/img/default.png", "/static/yunj/img/default.png"],
                "file" => ["name" => "default.png", "url" => "/static/yunj/img/default.png"],
                "files" => [["name" => "default.png", "url" => "/static/yunj/img/default.png"], ["name" => "bg.png", "url" => "/static/yunj/img/bg.png"]],
                "color" => "#fff000",
            ],
            [
                "id" => 3,
                "text1" => "测试内容1-3",
                "text2" => "测试内容2-3",
                "enum" => 1,
                "datetime" => "1636869353",
                "img" => "/static/yunj/img/default.png",
                "imgs" => ["/static/yunj/img/default.png", "/static/yunj/img/default.png"],
                "file" => ["name" => "default.png", "url" => "/static/yunj/img/default.png"],
                "files" => [["name" => "default.png", "url" => "/static/yunj/img/default.png"], ["name" => "bg.png", "url" => "/static/yunj/img/bg.png"]],
                "color" => "#fff000",
            ],
            [
                "id" => 4,
                "text1" => "测试内容1-4",
                "text2" => "测试内容2-4",
                "enum" => 1,
                "datetime" => "1636869353",
                "img" => "/static/yunj/img/default.png",
                "imgs" => ["/static/yunj/img/default.png", "/static/yunj/img/default.png"],
                "file" => ["name" => "default.png", "url" => "/static/yunj/img/default.png"],
                "files" => [["name" => "default.png", "url" => "/static/yunj/img/default.png"], ["name" => "bg.png", "url" => "/static/yunj/img/bg.png"]],
                "color" => "#fff000",
            ],
            [
                "id" => 5,
                "text1" => "测试内容1-5",
                "text2" => "测试内容2-5",
                "enum" => 1,
                "datetime" => "1636869353",
                "img" => "/static/yunj/img/default.png",
                "imgs" => ["/static/yunj/img/default.png", "/static/yunj/img/default.png"],
                "file" => ["name" => "default.png", "url" => "/static/yunj/img/default.png"],
                "files" => [["name" => "default.png", "url" => "/static/yunj/img/default.png"], ["name" => "bg.png", "url" => "/static/yunj/img/bg.png"]],
                "color" => "#fff000",
            ]
        ];

        return $items;
    }


}