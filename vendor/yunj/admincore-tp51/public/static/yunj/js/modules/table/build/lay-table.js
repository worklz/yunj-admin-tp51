/**
 * TableBuildLayTable
 */
layui.define(['jquery', 'yunj', "TableBuild", 'table'], function (exports) {

    let win = window;
    let doc = document;
    let $ = layui.jquery;
    let table = layui.table;
    let TableBuild = layui.TableBuild;

    class TableBuildLayTable extends TableBuild {

        constructor(table) {
            super(table, "layTable");

            this.fieldSort = {};                // 表格字段排序

            this.itemsCount = 0;                // 当前表格数据项数量

            this.items = [];                    // 当前表格数据项

            this.layTableId = `yunj_table_${this.tableId}`;     // 当前lay表格id

            this.layTable = null;               // 当前lay表格对象

            this.layTableCurrTd = null;         // 当前lay表格操作格子，用于触发工具栏展开绑定事件无效

            this._initLayTable();
        }

        // 初始化build
        _initBuild() {
        }

        // 初始化
        _initLayTable() {
            let that = this;
            if (!that.isSetCols()) throw new Error(`表格[${that.tableId}]未设置[cols]`);

            let buildBoxElClass = "yunj-table-lay-table-box";
            that.tableBoxEl.append(`<div class="${buildBoxElClass}" style="padding-top: 0"><table class="layui-table" id="${that.layTableId}" lay-filter="${that.layTableId}"></table></div>`);
            that.buildBoxEl = that.tableBoxEl.find(`.${buildBoxElClass}`);
        }


        // 渲染
        async render() {
            let that = this;
            that._renderBefore();
            let rawArgs = that.table.rawArgs;
            let state = that.isSetState() ? that.getCurrState() : null;
            let filter = that.getCurrRequestFilter();
            let args = {
                elem: '#' + that.layTableId,
                url: that.table.url,
                method: 'post',
                where: {
                    builderId: that.tableId,
                    builderAsyncType: 'items',
                    filter: filter,
                    sort: that.getCurrRequestSort()
                },
                contentType: 'application/json',
                parseData: function (res) {
                    let data = res.data;
                    let items = yunj.isObj(data) && data.hasOwnProperty('items') ? data.items : [];
                    items.map(item => {
                        item.is_export = false;
                        return item;
                    });
                    that.items = items;
                    return {
                        "code": res.errcode,
                        "msg": res.msg,
                        "count": that.itemsCount,
                        "data": that.items
                    };
                },
                loading: true,
                text: {none: '暂无相关数据'},
                autoSort: false,
                cols: [],
                done: function (res) {
                    table.resize(this.id);
                    // 渲染表格完成事件触发
                    $(doc).trigger(`yunj_table_${that.tableId}_render_table_done`);
                }
            };

            // page
            if (that.isSetPage()) {
                if (that.isSetState()) {
                    args.page = yunj.isObj(rawArgs.page) && rawArgs.page.hasOwnProperty(state) ? rawArgs.page[state] : true;
                } else {
                    args.page = yunj.isBool(rawArgs.page) ? rawArgs.page : true;
                }
                // limit
                if (that.isSetLimit()) {
                    if (that.isSetState()) {
                        args.limit = yunj.isObj(rawArgs.limit) && rawArgs.limit.hasOwnProperty(state) ? rawArgs.limit[state] : 20;
                    } else {
                        args.limit = yunj.isPositiveInt(rawArgs.limit) ? rawArgs.limit : 20;
                    }
                }
                // limits
                if (that.isSetLimits()) {
                    if (that.isSetState()) {
                        args.limits = yunj.isObj(rawArgs.limits) && rawArgs.limits.hasOwnProperty(state) ? rawArgs.limits[state] : [10, 20, 30, 40, 50, 60, 70, 80, 90];
                    } else {
                        args.limits = yunj.isPositiveIntArray(rawArgs.limits) ? rawArgs.limits : [10, 20, 30, 40, 50, 60, 70, 80, 90];
                    }
                }
            }

            if (!yunj.isEmptyObj(that.fieldSort)) {
                let field = Object.keys(that.fieldSort)[0];
                args.initSort = {field: field, type: that.fieldSort[field]}
            }

            // 表格头部工具栏
            if (that.isSetToolbar()) await that._setTableArgsByToolbar(args);

            // 表格头部右侧工具栏
            if (that.isSetDefaultToolbar()) that._setTableArgsByDefaultToolbar(args);

            // 表头
            await that._setTableArgsByCols(args);

            // 数据量
            await that._setItemsCount(filter);

            console.log(args);

            that.layTable = table.render(args);

            that._renderAfter();
        }

        // 设置表格头部工具栏参数
        async _setTableArgsByToolbar(args) {
            let that = this;
            let rawArgs = that.table.rawArgs;

            let tableToolbarOptions = {
                tableId: that.tableId,
                key: 'toolbar',
            };
            if (that.isSetState()) {
                let state = that.getCurrState();
                if (!rawArgs.toolbar.hasOwnProperty(state)) return;
                tableToolbarOptions.state = state;
                tableToolbarOptions.options = rawArgs.toolbar[state];
            } else {
                tableToolbarOptions.options = rawArgs.toolbar;
            }
            await new Promise(resolve => {
                layui.use("tableToolbar", () => {
                    layui.tableToolbar(tableToolbarOptions).render().then(obj => {
                        args.toolbar = `#${obj.id}`;
                        resolve();
                    });
                });
            });
        }

        // 设置表格头部右侧工具栏参数
        _setTableArgsByDefaultToolbar(args) {
            let that = this;
            let rawArgs = that.table.rawArgs;
            let state = that.getCurrState();
            let defaultToolbar = [];
            let rawDefaultToolbar = [];
            if (that.isSetState()) {
                if (!rawArgs.defaultToolbar.hasOwnProperty(state)) return;
                rawDefaultToolbar = rawArgs.defaultToolbar[state];
            } else {
                rawDefaultToolbar = rawArgs.defaultToolbar;
            }

            for (let i = 0, l = rawDefaultToolbar.length; i < l; i++) {
                let k = rawDefaultToolbar[i];
                if (k === "export") {
                    defaultToolbar.push({title: '数据导出', layEvent: 'export', icon: 'layui-icon-export'});
                } else if (k === "import" && that.isSetImport() && !yunj.isMobile()) {
                    defaultToolbar.push({title: '数据导入', layEvent: 'import', icon: 'layui-icon-upload'});
                } else {
                    // filter、print
                    defaultToolbar.push(rawDefaultToolbar[i]);
                }
            }

            if (defaultToolbar.length > 0) {
                if (!args.hasOwnProperty('toolbar')) args.toolbar = true;
                args.defaultToolbar = defaultToolbar;
            }
        }

        // 设置表格表头参数
        async _setTableArgsByCols(args) {
            let that = this;
            let rawArgs = that.table.rawArgs;
            let state = that.getCurrState();
            let cols = [];
            let rawCols = [];
            if (that.isSetState()) {
                if (!rawArgs.cols.hasOwnProperty(state)) return;
                rawCols = rawArgs.cols[state];
            } else {
                rawCols = rawArgs.cols;
            }

            for (let k in rawCols) {
                if (!rawCols.hasOwnProperty(k)) continue;
                let rawCol = rawCols[k];
                let col = yunj.objSupp(rawCol, {
                    field: '',
                    title: '',
                    type: 'normal',
                    align: 'left',
                    sort: false,
                    fixed: false,
                    hide: false,
                    templet: '',
                });
                // 处理hide
                switch (rawCol.hide) {
                    case 'yes':
                        col.hide = true;
                        break;
                    case 'mobileHide':
                        col.hide = yunj.isMobile();
                        break;
                    default:
                        col.hide = false;
                }
                // 处理templet
                let rawTemplet = rawCol['templet'];
                if (rawTemplet.length > 0) {
                    let templetArgs = {
                        tableId: that.tableId,
                        key: rawCol.field,
                        args: rawCol,
                    };
                    if (that.isSetState()) templetArgs.state = state;

                    await new Promise(resolve => {
                        yunj.tableCol(rawTemplet, templetArgs).then(colObj => {
                            colObj.render().then(res => {
                                col['templet'] = `#${colObj.id}`;
                                resolve();
                            });
                        });
                    });
                }

                if (rawTemplet === 'action') col.minWidth = 80;
                cols.push(col);
            }
            args.cols = [cols];
        }

        // 设置当前条件下的表格数据量
        _setItemsCount(filter = null) {
            let that = this;
            filter = filter || that.getCurrRequestFilter();
            return new Promise(resolve => {
                yunj.request(that.table.url, {
                    builderId: that.tableId,
                    builderAsyncType: 'count',
                    filter: filter
                }, "post").then(res => {
                    let count = res.data.count;
                    that.itemsCount = yunj.isPositiveInteger(count) ? count : 0;
                    resolve();
                }).catch(e => {
                    yunj.error(e);
                });
            });
        }

        // 渲染前
        _renderBefore() {
            let that = this;

            // 绑定获取请求filter data的触发事件
            let eventRepeatKey = `YUNJ_TABLE_${that.tableId}_GET_REQUEST_FILTER_DATA_EVENT_BIND_BY_LAY_TABLE`;
            if (yunj.isUndefined(win[eventRepeatKey])) {
                win[eventRepeatKey] = true;
                $(doc).bind(`yunj_table_${that.tableId}_get_request_filter_data`, function (e, data, args) {
                    if (!args.ids) return;
                    data.ids = table.checkStatus(that.layTableId).data.map(row => {
                        return row.id
                    });
                });
            }
        }

        // 渲染后
        _renderAfter() {
        }

        // 因为数据变化产生的渲染
        async renderByDataChange() {
            let that = this;
            if (that.isSetState()) await that.table.buildMap.state.renderCount();
            await that.render();
        }

        /**
         * 重载
         * @param {boolean} isFirstPage [是否从第一页开始]
         */
        reload(isFirstPage = true) {
            let that = this;
            let args = {
                where: {
                    id: that.tableId,
                    type: 'items',
                    filter: that.getCurrRequestFilter(),
                    sort: that.getCurrRequestSort()
                }
            };
            if (isFirstPage) args.page = {curr: 1};
            that.layTable.reload(args);
        }

        setEventBind() {
            let that = this;

            // 绑定筛选表单提交的触发事件
            $(doc).bind(`yunj_table_${that.tableId}_filter_submit`, function (e) {
                that.render();
            });

            // 当前展开格子
            let target = `.yunj-table-lay-table-box div[lay-id=${that.layTableId}] .layui-table-grid-down`;
            $(doc).off('mousedown', target).on('mousedown', target, function () {
                that.layTableCurrTd = $(this).closest('td');
            });

            // 展开格子事件
            target = `.layui-table-tips-main .layui-btn-container[yunj-id=${that.tableId}] [lay-event]`
            $(doc).off('click', target).on('click', target, function () {
                if (!that.layTableCurrTd) return;
                let el = $(this);
                let layEvent = el.attr('lay-event');
                let layerIdx = el.closest('.layui-table-tips').attr('times');
                layer.close(layerIdx);
                that.layTableCurrTd.find(`[lay-event=${layEvent}]`).first().click();
            });

            // 监听工具栏事件
            if (that.isSetToolbar() || that.isSetDefaultToolbar()) {
                table.on(`toolbar(${that.layTableId})`, function (obj) {
                    let el = $(this);
                    let args = el.data('args');
                    switch (obj.event) {
                        case 'openPopup':
                            let popupUrl = yunj.urlPushParam(args.url, {
                                rawTable: that.tableId,
                            });
                            yunj.openPopup(popupUrl, args.title, true);
                            break;
                        case 'openTab':
                            let tabUrl = yunj.urlPushParam(args.url, {
                                rawTable: that.tableId,
                            });
                            yunj.openTab(tabUrl, args.title, true);
                            break;
                        case 'asyncEvent':
                            args.ids = table.checkStatus(obj.config.id).data.map(row => {
                                return row.id
                            });
                            that.asyncEventRequest(args);
                            break;
                        case 'export':
                            yunj.tableExport(that.table);
                            break;
                        case 'import':
                            let imporRaw = that.table.rawArgs.import;
                            let importUrl = that.isSetState() ? imporRaw[that.getCurrState()] : imporRaw;
                            importUrl = yunj.urlPushParam(importUrl, {
                                rawTable: that.tableId,
                            });
                            yunj.openPopup(importUrl, '数据导入', true);
                            break;
                    }
                });
            }

            // 监听表格内操作列事件
            table.on(`tool(${that.layTableId})`, function (obj) {
                let el = $(this);
                let args = el.data('args');
                let rowData = obj.data;
                let rowId = rowData.id;
                let openPageTitle = args.title + " ID:" + rowId;
                switch (obj.event) {
                    case 'openPopup':
                        let popupUrl = yunj.urlPushParam(args.url, {
                            id: rowId,
                            rawTable: that.tableId,
                        });
                        yunj.openPopup(popupUrl, openPageTitle, true);
                        break;
                    case 'openTab':
                        let tabUrl = yunj.urlPushParam(args.url, {
                            id: rowId,
                            rawTable: that.tableId,
                        });
                        yunj.openTab(tabUrl, openPageTitle, true);
                        break;
                    case 'asyncEvent':
                        args.ids = [rowId];
                        that.asyncEventRequest(args, that);
                        break;
                }
            });

            // 监听表头排序事件
            table.on(`sort(${that.layTableId})`, function (obj) {
                that.fieldSort = ['asc', 'desc'].indexOf(obj.type) === -1 ? {} : {[obj.field]: obj.type};
                that.reload(false);
            });
        }

        /**
         * 请求条件sort
         * @returns {string}
         */
        getCurrRequestSort() {
            return JSON.stringify(this.fieldSort);
        }

        // 表格异步事件请求
        asyncEventRequest(param) {
            let that = this;
            param = yunj.objSupp(param, {title: '操作请求', confirmText: null, event: '', url: '', ids: []});
            if (!yunj.isArray(param.ids) || yunj.isEmptyArray(param.ids)) {
                yunj.msg('请勾选要操作的数据列');
                return false;
            }
            let filter = {event: param.event, ids: param.ids};
            let requestParam = {builderId: that.tableId, builderAsyncType: 'event', filter: filter};

            let eventRequest = () => {
                yunj.request((param.url ? param.url : that.url), requestParam, "post").then(res => {
                    let requestData = requestParam.filter;
                    let event = requestData.event;
                    if (yunj.isObj(res.data) && res.data.hasOwnProperty('reload') && !res.data.reload) {
                        that.asyncEventRequestDone(event, requestData, res);
                        return;
                    }
                    that.renderByDataChange().then(() => {
                        that.asyncEventRequestDone(event, requestData, res);
                    });
                }).catch(e => {
                    console.log(e);
                    yunj.error(`${param.title}失败`);
                });
            };

            if (param.confirmText)
                yunj.confirm(param.confirmText, function () {
                    eventRequest();
                });
            else
                eventRequest();
        }

        // 异步事件请求完成
        asyncEventRequestDone(event, requestData, responseRes) {
            let that = this;
            $(doc).trigger(`yunj_table_${that.tableId}_event_done`, [event, requestData, responseRes]);
            $(doc).trigger(`yunj_table_${that.tableId}_event_${event}_done`, [requestData, responseRes]);
        }

    }

    exports('TableBuildLayTable', TableBuildLayTable);
});