/**
 * YunjButton
 */
layui.define(['jquery'], function (exports) {
    let win = window;
    let doc = document;
    let $ = layui.jquery;

    class YunjButton {

        constructor(form_id) {
            this.version = '1.0.0';         // 版本号

            this.type='';                   // 类型

            this.title='';                  // 标题
            this.class='layui-btn-primary'; // class标记
            this.icon='';                   // 标题
            this.form_id=form_id;           // 表单id

            this.init();
        }

        init() {
            let that = this;
            // 设置数据
            that.set_data();
        }

        // 设置数据
        set_data() {
            let that = this;
            that.set_type();
        }

        // 设置type
        set_type(){
            let that = this;
            let type=that.constructor.name;
            type=type.replace('Yunj','').toLowerCase();
            that.type=type;
        }

        // 生成id
        generate_id(){
            let that = this;
            return that.form_id+'_'+that.type;
        }

        /**
         * 渲染
         * @param box_filter    [渲染所在容器的filter]
         * @param out_layout    [外部结构]
         * 当传入外部结构时，如：<div>__layout__</div>，会用生成的结构替换掉关键词 __layout__
         */
        render(box_filter='',out_layout=''){
            let that=this;
            let layout=that.layout();
            if(yunj.isString(out_layout)&&out_layout.indexOf('__layout__')!==-1) layout=out_layout.replace('__layout__',layout);
            $(box_filter).append(layout);
            // 渲染后执行
            that.render_done();

            return that;
        }

        // 渲染后执行
        render_done(){}

        /**
         * 布局
         * @param is_dd
         * @returns {string}
         */
        layout(is_dd=false){
            let that = this;

            let id=that.generate_id();
            let icon=that.icon?`<i class="${that.icon}"></i> `:'';
            return is_dd?`<dd id="${id}" title="${that.title}" class="yunj-btn-${that.type}">${that.title}</dd>`
                :`<button type="button" class="layui-btn layui-btn-sm ${that.class} yunj-btn-${that.type}" id="${id}" title="${that.title}">${icon} ${that.title}</button>`;
        }

    }

    class YunjBack extends YunjButton{

        constructor(form_id){
            super(form_id);
            this.title='返回';
            this.icon='yunj-icon yunj-icon-back';
        }

        set_type(){
            let that = this;
            that.type='return';
        }

    }

    class YunjClear extends YunjButton{

        constructor(form_id){
            super(form_id);
            this.title='清空';
            this.icon='yunj-icon yunj-icon-clear';
        }

        set_type(){
            let that = this;
            that.type='clear';
        }

    }

    class YunjReload extends YunjButton{

        constructor(form_id){
            super(form_id);
            this.title='重载';
            this.icon='yunj-icon yunj-icon-refresh';
        }

        set_type(){
            let that = this;
            that.type='reload';
        }

    }

    class YunjReset extends YunjButton{

        constructor(form_id){
            super(form_id);
            this.title='重置';
            this.icon='layui-icon layui-icon-refresh';
        }

        set_type(){
            let that = this;
            that.type='reset';
        }

    }

    class YunjSearch extends YunjButton{

        constructor(form_id){
            super(form_id);
            this.title='搜索';
            this.class='';
            this.icon='layui-icon layui-icon-search';
        }

        set_type(){
            let that = this;
            that.type='search';
        }

    }

    class YunjSubmit extends YunjButton{

        constructor(form_id){
            super(form_id);
            this.title='提交';
            this.class='';
            this.icon='yunj-icon yunj-icon-submit';
        }

        set_type(){
            let that = this;
            that.type='submit';
        }

    }

    let button = {
        back: (form_id) => {
            return new YunjBack(form_id);
        },
        clear: (form_id) => {
            return new YunjClear(form_id);
        },
        reset: (form_id) => {
            return new YunjReset(form_id);
        },
        reload: (form_id) => {
            return new YunjReload(form_id);
        },
        search: (form_id) => {
            return new YunjSearch(form_id);
        },
        submit: (form_id) => {
            return new YunjSubmit(form_id);
        },
    };

    exports('button', button);
});