<?php
namespace yunj\control\cols\templet;

use yunj\control\cols\YunjCols;

class File extends YunjCols {

    private static $instance;

    public static function instance(){
        if (!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

}