<?php

namespace yunj\control;

use yunj\Config;
use yunj\control\field\YunjField;

final class YunjControl {

    // 工具栏
    private $toolbarArgsArr;

    // 表格表头type
    private $colsTypeArgsArr;

    // 表格表头templet
    private $colsArgsArr;

    public $colsModuleArr;

    // 表单字段type
    private $fieldArgsArr;

    public $fieldModuleArr;

    // 系统配置
    private $systemConfig;

    // 自定义配置
    private $customConfig;

    private function __construct() {
        $this->systemConfig = Config::get('control.', []);
        $this->customConfig = yunj_config('control.', []);
        $this->setAttr();
    }

    private function setAttr() {
        // toolbar
        $this->setAttrToolbar();
        // cols_type
        $this->setAttrColsType();
        // cols
        $this->setAttrCols();
        // fields
        $this->setAttrFields();
    }

    private function setAttrToolbar() {
        $this->toolbarArgsArr = [
            'normal' => '\\yunj\\control\\toolbar\\Normal',
            'dropdown' => '\\yunj\\control\\toolbar\\Dropdown',
        ];
    }

    private function setAttrColsType() {
        $this->colsTypeArgsArr = [
            'normal' => '\\yunj\\control\\cols\\type\\Normal',
            'checkbox' => '\\yunj\\control\\cols\\type\\CheckBox',
        ];
    }

    private function setAttrCols() {
        // 系统配置
        foreach ($this->systemConfig['cols'] as $k => $v) {
            $this->colsArgsArr[$k] = $v['args'];
            $this->colsModuleArr["TableCol".ucfirst($k)] = $v['module'];
        }
        // 自定义配置
        foreach ($this->customConfig['cols'] as $k => $v) {
            if (isset($this->colsArgsArr[$k])) continue;
            $this->colsArgsArr[$k] = $v['args'];
            $this->colsModuleArr["TableCol".ucfirst($k)] = $v['module'];
        }
    }

    private function setAttrFields() {
        // 系统配置
        foreach ($this->systemConfig['fields'] as $k => $v) {
            $this->fieldArgsArr[$k] = $v['args'];
            $this->fieldModuleArr["FormField".ucfirst($k)] = $v['module'];
        }
        // 自定义配置
        foreach ($this->customConfig['fields'] as $k => $v) {
            if (isset($this->fieldArgsArr[$k])) continue;
            $this->fieldArgsArr[$k] = $v['args'];
            $this->fieldModuleArr["FormField".ucfirst($k)] = $v['module'];
        }
    }

    private static $instance;

    public static function instance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Notes: 字段实例
     * Author: Uncle-L
     * Date: 2020/9/25
     * Time: 10:57
     * @param string $key
     * @param array $args
     * @return YunjField|null
     */
    public static function field(String $key, array $args) {
        $args['type'] = isset($args['type']) ? $args['type'] : 'text';
        $fieldClass = self::instance()->fieldArgsArr[$args['type']];
        $field = $fieldClass::instance();
        if (!($field instanceof YunjField)) return null;
        return $field->setAttr($key, $args);
    }

    /**
     * Notes: 表头实例
     * Author: Uncle-L
     * Date: 2020/9/25
     * Time: 10:56
     * @param string $key
     * @param array $args
     * @return mixed
     */
    public static function col(String $key, array $args) {
        $allowTypeArr = self::instance()->colsTypeArgsArr;
        $args['type'] = isset($args['type']) && isset($allowTypeArr[$args['type']]) ? $args['type'] : 'normal';

        // type!=normal
        if ($args['type'] != 'normal') {
            if (isset($args['templet'])) unset($args['templet']);
            return $allowTypeArr[$args['type']]::instance()->setAttr($key, $args);
        }

        // templet
        if (isset($args['templet'])) {
            $allowTempletArr = self::instance()->colsArgsArr;
            if (isset($allowTempletArr[$args['templet']])) return $allowTempletArr[$args['templet']]::instance()->setAttr($key, $args);
        }

        // 始终返回常规的表头
        return $allowTypeArr['normal']::instance()->setAttr($key, $args);
    }

    /**
     * Notes: 工具栏操作实例
     * Author: Uncle-L
     * Date: 2020/9/25
     * Time: 10:54
     * @param string $key
     * @param array $args
     * @return mixed
     */
    public static function toolbar(String $key, array $args) {
        $allowToolbarArr = self::instance()->toolbarArgsArr;
        $toolbar = isset($args['dropdown']) && $args['dropdown'] ? $allowToolbarArr['dropdown'] : $allowToolbarArr['normal'];
        return $toolbar::instance()->setAttr($key, $args);
    }

}