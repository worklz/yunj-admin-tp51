<?php
namespace yunj;

final class YunjArea{

    private $path;

    private function __construct() {
        $this->path=env('root_path').'public/static/yunj/json/area.json';
    }

    private static $instance;

    public static function instance(){
        if (!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function options(){
        static $options;
        if($options) return $options;
        $optionStr=file_get_contents($this->path);
        $options=json_decode($optionStr,true);
        return $options;
    }

}