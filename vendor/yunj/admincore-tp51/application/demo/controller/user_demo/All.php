<?php

namespace app\demo\controller\user_demo;

use app\demo\enum\Sex;
use app\demo\enum\Grade;
use app\demo\enum\Hobby;
use app\demo\controller\Controller;
use app\demo\service\user_demo\All as Service;

class All extends Controller {

    /**
     * @return mixed|\think\response\Json
     * @throws \Exception
     */
    public function lists() {
        $builder = YT("demo")
            ->filter(['name' => ['title' => '姓名']])
            ->toolbar(['add' => ['type' => 'openPopup', 'title' => '添加', 'class' => 'layui-icon layui-icon-add-circle', 'url' => url('add')]])
            ->defaultToolbar('filter', 'export', 'print', 'import')
            ->import(url("import"))
            ->cols([
                'id' => [
                    'type' => 'checkbox',
                    'fixed' => "left"
                ],
                'avatar' => [
                    'title' => '头像',
                    "templet" => "img",
                    "align" => "center"
                ],
                'name' => ['title' => '姓名'],
                'sex' => [
                    'title' => '性别',
                    "templet" => "enum",
                    "options" => Sex::getTitleMap(),
                    "align" => "center"
                ],
                'age' => [
                    'title' => '年龄',
                    "align" => "center"
                ],
                'grade' => [
                    'title' => '年级',
                    "templet" => "enum",
                    "options" => Grade::getTitleMap(),
                    "align" => "center"
                ],
                'color' => [
                    'title' => '喜欢的颜色',
                    "templet" => "color"
                ],
                "album" => [
                    'title' => '相册',
                    "templet" => "imgs"
                ],
                "hobby" => [
                    'title' => '爱好',
                    "templet" => "enum",
                    "options" => Hobby::getTitleMap()
                ],
                "interest_cate_ids" => [
                    'title' => '兴趣分类',
                    "templet" => "enum",
                    "options" => Service::getInstance()->getInterestCateIdNameOptions()
                ],
                'area' => [
                    'title' => '地址',
                    'templet' => 'area',
                    'hide' => 'yes',
                ],
                'document' => [
                    'title' => '个人文档',
                    'templet' => 'file',
                    'hide' => 'yes',
                ],
                'other_document' => [
                    'title' => '其他文档',
                    'templet' => 'files',
                    'hide' => 'yes',
                ],
                'create_time' => [
                    'title' => '添加时间',
                    'align' => 'center',
                    'templet' => 'datetime',
                    'hide' => 'yes',
                ],
                'last_update_time' => [
                    'title' => '上次更新时间',
                    'align' => 'center',
                    'templet' => 'datetime'
                ],
                'action' => [
                    'title' => '操作',
                    'fixed' => "right",
                    'templet' => 'action',
                    'options' => [
                        'edit' => ['type' => 'openPopup', 'title' => '详情', 'icon' => 'layui-icon layui-icon-survey', 'url' => url('edit')],
                    ]
                ]
            ])
            ->count(function ($filter) {
                return Service::getInstance()->count($filter);
            })
            ->items(function ($limitStart, $limitLength, $filter, $sort) {
                return Service::getInstance()->items($limitStart, $limitLength, $filter, $sort);
            });
        return view_table($builder);
    }

    /**
     * @return mixed|\think\response\Json
     * @throws \Exception
     */
    public function add() {
        $builder = YF('demo')
            ->field([
                "avatar" => [
                    "title" => "头像",
                    "type" => "img",
                ],
                "name" => [
                    "title" => "姓名",
                    "type" => "text",
                    "default" => "某某人",
                    "verify" => "require|chsDash",
                    "desc" => '必须，允许键入汉字/字母/数字/下划线/破折号-'
                ],
                "sex" => [
                    "title" => "性别",
                    "type" => "radio",
                    "options" => Sex::getTitleMap(),
                    "verify" => "require",
                ],
                "age" => [
                    "title" => "年龄",
                    "default" => 18,
                    "verify" => "require|positiveInt",
                ],
                "grade" => [
                    "title" => "年级",
                    "type" => "select",
                    "options" => Grade::getTitleMap(),
                ],
                "hobby" => [
                    "title" => "爱好",
                    "type" => "checkbox",
                    "options" => Hobby::getTitleMap()
                ],
                "area" => [
                    "title" => "地址",
                    "type" => "area",
                    "verify" => "require",
                ],
                "intro" => [
                    "title" => "个人介绍",
                    "type" => "textarea",
                    "default" => "某某人的个人介绍",
                    "verify" => "max:200",
                    "desc" => '限制200字符'
                ],
                "interest_cate_ids" => [
                    "title" => "感兴趣的分类",
                    'type' => 'tree',
                    'nodes' => Service::getInstance()->getAllInterestCateItems()
                ],
                "friend_ids" => [
                    "title" => "朋友",
                    'type' => 'dropdownSearch',
                    'verify' => 'arrayPositiveInt',
                    'url' => url('userDropdownSearchOptions'),
                ],
                "color" => [
                    "title" => "喜欢的颜色",
                    "type" => "color",
                ],
                "album" => [
                    "title" => "相册",
                    "type" => "imgs",
                ],
                'document' => [
                    'title' => '个人文档',
                    'type' => 'file',
                ],
                'other_document' => [
                    'title' => '其他文档',
                    'type' => 'files',
                ],
            ])
            ->button('reload', 'reset', 'submit')
            ->submit(function ($data) {
                $res = Service::getInstance()->submit($data);
                return $res ? success_json(['reload' => true]) : error_json();
            });
        return view_form($builder);
    }

    /**
     * @return mixed|\think\response\Json
     * @throws \Exception
     */
    public function edit() {
        $builder = YF('demo')
            ->field([
                "id" => [
                    "title" => "用户",
                    "type" => "hidden",
                    "verify" => "require|positiveInt",
                ],
                "avatar" => [
                    "title" => "头像",
                    "type" => "img",
                ],
                "name" => [
                    "title" => "姓名",
                    "type" => "text",
                    "default" => "某某人",
                    "verify" => "require|chsDash",
                    "desc" => '必须，允许键入汉字/字母/数字/下划线/破折号-'
                ],
                "sex" => [
                    "title" => "性别",
                    "type" => "radio",
                    "options" => Sex::getTitleMap(),
                    "verify" => "require",
                ],
                "age" => [
                    "title" => "年龄",
                    "default" => 18,
                    "verify" => "require|positiveInt",
                ],
                "grade" => [
                    "title" => "年级",
                    "type" => "select",
                    "options" => Grade::getTitleMap(),
                ],
                "hobby" => [
                    "title" => "爱好",
                    "type" => "checkbox",
                    "options" => Hobby::getTitleMap()
                ],
                "area" => [
                    "title" => "地址",
                    "type" => "area",
                    "verify" => "require",
                ],
                "intro" => [
                    "title" => "个人介绍",
                    "type" => "textarea",
                    "default" => "某某人的个人介绍",
                    "verify" => "max:200",
                    "desc" => '限制200字符'
                ],
                "interest_cate_ids" => [
                    "title" => "感兴趣的分类",
                    'type' => 'tree',
                    'nodes' => Service::getInstance()->getAllInterestCateItems()
                ],
                "friend_ids" => [
                    "title" => "朋友",
                    'type' => 'dropdownSearch',
                    'verify' => 'arrayPositiveInt',
                    'url' => url('userDropdownSearchOptions'),
                ],
                "color" => [
                    "title" => "喜欢的颜色",
                    "type" => "color",
                ],
                "album" => [
                    "title" => "相册",
                    "type" => "imgs",
                ],
                'document' => [
                    'title' => '个人文档',
                    'type' => 'file',
                ],
                'other_document' => [
                    'title' => '其他文档',
                    'type' => 'files',
                ],
            ])
            ->button('reload', 'reset', 'submit')
            ->load(function () {
                return Service::getInstance()->load();
            })
            ->submit(function ($data) {
                $res = Service::getInstance()->submit($data, true);
                return $res ? success_json(['reload' => true]) : error_json("修改失败");
            });
        return view_form($builder);
    }

    /**
     * @return \think\response\View
     * @throws \Exception
     */
    public function import() {
        $builder = YI('demo', [
            "cols" => function () {
                $cols = [
                    'name' => [
                        "title" => '姓名',
                        "default" => "某某人",
                        "verify" => "require|chsDash",
                        "desc" => '必填，允许输入汉字/字母/数字/下划线/破折号-'
                    ],
                    'sex' => [
                        "title" => '性别',
                        'default' => '男',
                        'verify' => 'require|in:男,女',
                        "desc" => "必填，男/女",
                    ],
                    'grade' => [
                        "title" => '年级',
                        'default' => '一年级',
                        'verify' => 'require|in:'.implode(",",Grade::getTitleMap()),
                        "desc" => "必填，".implode("/",Grade::getTitleMap()),
                    ],
                    'age' => [
                        "title" => '年龄',
                        'default' => '18',
                        'verify' => 'require|positiveInt',
                        "desc" => "必填，正整数",
                    ],
                    "hobby" => [
                        "title" => "爱好",
                        'default' => '阅读',
                        'verify' => 'in:写作,阅读',
                        "desc" => "写作/阅读",
                    ],
                    'intro' => [
                        "title" => '简介',
                        'default' => '个人简介',
                        'verify' => 'max:200',
                        "desc" => "限制200字符，如：姓名、曾用名、出生日期、籍贯、出生地、民族、现有文化程度、家庭现住址、现在工作单位和担任的社会职务、有何专业技术职称等基本情况。",
                    ],
                ];
                return $cols;
            },
            "rows" => function ($rowsData) {
                return Service::getInstance()->rows($rowsData);
            }
        ]);
        return view_import($builder);
    }

    public function userDropdownSearchOptions() {
        $items = Service::getInstance()->userDropdownSearchOptions();
        return success_json($items);
    }

}