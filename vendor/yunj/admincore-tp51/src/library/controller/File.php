<?php

namespace yunj\controller;

use yunj\YunjFile;
use yunj\Controller;

final class File extends Controller {

    public function upload($from = 'input') {
        return YunjFile::instance()->upload($from);
    }

}