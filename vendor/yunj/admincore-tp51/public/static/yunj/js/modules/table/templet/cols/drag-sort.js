/**
 * TableColDragSort
 */
layui.define(['TableColTemplet', 'jquery', 'yunj'], function (exports) {

    let TableColTemplet = layui.TableColTemplet;
    let win = window;
    let doc = document;
    let $ = layui.jquery;

    class TableColDragSort extends TableColTemplet {

        constructor(options) {
            super(options);
            this.tableSortable = null;              // 当前表格数据排序对象
            this.tableSortableBoxFlag = null;       // 当前表格数据排序容器标识
        }

        async renderBefore() {
            let that = this;
            that.tableSortableBoxFlag = `div[lay-id=yunj_table_${that.tableId}] .layui-table-box .layui-table-main tbody`;
            await yunj.includeJs('/static/yunj/libs/Sortable/Sortable.min.js');
            return 'done'
        }

        layout() {
            let that = this;
            return `{{#  
                        let val = d.${that.key};
                        let content = "";
                        if(yunj.isUndefined(val)){
                            content = "<i class='show ${that.args.iconClass}'></i>";
                        }else{
                            content = "<i class='hide ${that.args.iconClass}'></i><div class='content' title='"+val+"'>"+val+"</div>";
                        }
                     }}
                    <div class="yunj-table-drag-sort-item" data-id="{{ d.id }}" title="拖拽排序">{{ content }}</div>`;
        }

        defineExtraEventBind() {
            let that = this;

            // 防止重复绑定事件
            if (yunj.isUndefined(win.TABLE_ROW_DRAG_SORT_RENDER_TABLE_DONE_EVENT_BIND)) {
                win.TABLE_ROW_DRAG_SORT_RENDER_TABLE_DONE_EVENT_BIND = true;
                $(doc).bind(`yunj_table_${that.tableId}_render_table_done`, function () {
                    let sortableConfig = {
                        handle: `.yunj-table-drag-sort-item`,
                        animation: 150,
                        ghostClass: 'yunj-table-drag-sort-item-checked',
                        onUpdate: function (evt) {
                            that.tableSortExec();
                        },
                    };
                    that.tableSortable = new Sortable(doc.querySelector(that.tableSortableBoxFlag), sortableConfig);
                });
            }

            if (yunj.isUndefined(win.TABLE_ROW_COLOR_CLICK_COPY_EVENT_BIND)) {
                win.TABLE_ROW_COLOR_CLICK_COPY_EVENT_BIND = true;
                $(doc).on('click', '.yunj-table-drag-sort-item .content', function (e) {
                    yunj.copy($(this).attr('title'));
                    e.stopPropagation();
                });
            }
        }

        // 排序执行
        tableSortExec() {
            let that = this;
            that.tableSortable.option("disabled", true);
            let filter = Object.assign({}, {event: 'sort', ids: []}, that.table.requestFilter({convert: false}));
            $(`${that.tableSortableBoxFlag} .yunj-table-drag-sort-item`).each(function () {
                filter.ids.push($(this).data('id'));
            });
            let requestData = {builderId: that.tableId, builderAsyncType: 'event', filter: JSON.stringify(filter)};
            
            yunj.request(that.url, requestData, "post").then(res => {
                that.tableSortable.option("disabled", false);
                that.table.renderTableHandle();
            }).catch(e => {
                yunj.error("数据排序失败");
            });
            
        }

    }

    exports('TableColDragSort', TableColDragSort);
});