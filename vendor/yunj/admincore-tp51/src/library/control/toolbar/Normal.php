<?php
namespace yunj\control\toolbar;

class Normal extends YunjToolbar {

    private static $instance;

    public static function instance(){
        if (!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

}