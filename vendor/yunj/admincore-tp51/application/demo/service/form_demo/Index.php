<?php

namespace app\demo\service\form_demo;

use app\demo\model\User as UserModel;

final class Index {

    /**
     * @var UserModel
     */
    private $userModel;

    /**
     * @return UserModel
     */
    public function getUserModel(): UserModel {
        if (!$this->userModel) $this->setUserModel();
        return $this->userModel;
    }

    /**
     * @param UserModel $userModel
     */
    public function setUserModel(UserModel $userModel = null): void {
        $this->userModel = $userModel ?: (new UserModel());
    }

    private static $instance;

    public static function getInstance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Notes: 切换栏
     * Author: Uncle-L
     * Date: 2021/11/13
     * Time: 18:15
     * @return array
     */
    public function tab(): array {
        return ["normal" => "常规", "require" => "必填", "default" => "默认", "disabled" => "禁用"];
    }

    /**
     * Notes: 表单字段
     * Author: Uncle-L
     * Date: 2021/11/13
     * Time: 18:16
     * @param $tab
     * @return array
     */
    public function field($tab): array {
        $field = [];
        $tabField = [
            "hidden" => [
                "title" => "隐藏域",
                "type" => "hidden",
            ],
            "text" => [
                "title" => "单行文本框",
                "type" => "text",
            ],
            "textarea" => [
                "title" => "多行文本框",
                "type" => "textarea",
            ],
            "password" => [
                "title" => "密码框",
                "type" => "password",
            ],
            "editor" => [
                "title" => "富文本",
                "type" => "editor",
            ],
            "markdown" => [
                "title" => "Markdown",
                "type" => "markdown",
            ],
            "select" => [
                "title" => "下拉选框",
                "type" => "select",
                "options" => ["option1" => "选项一", "option2" => "选项二"],
            ],
            "radio" => [
                "title" => "单选框",
                "type" => "radio",
                "options" => ["option1" => "选项一", "option2" => "选项二"],
            ],
            "checkbox" => [
                "title" => "复选框",
                "type" => "checkbox",
                "options" => ["option1" => "选项一", "option2" => "选项二"],
            ],
            "switch" => [
                "title" => "开关",
                "type" => "switch",
                "text" => "开|关",
            ],
            "date" => [
                "title" => "日期",
                "type" => "date",
            ],
            "datetime" => [
                "title" => "时间日期",
                "type" => "datetime",
            ],
            "year" => [
                "title" => "年份",
                "type" => "year",
            ],
            "month" => [
                "title" => "月份",
                "type" => "month",
            ],
            "time" => [
                "title" => "时间",
                "type" => "time",
            ],
            "img" => [
                "title" => "单图上传",
                "type" => "img",
            ],
            "imgs" => [
                "title" => "多图上传",
                "type" => "imgs",
            ],
            "file" => [
                "title" => "单文件上传",
                "type" => "file",
            ],
            "files" => [
                "title" => "多文件上传",
                "type" => "files",
            ],
            "color" => [
                "title" => "取色器",
                "type" => "color",
            ],
            "area" => [
                "title" => "地区联动",
                "type" => "area",
            ],
            "dropdownSearch" => [
                "title" => "下拉搜索",
                "type" => "dropdownSearch",
                "url" => url('dropdownSearchOptions')
            ],
            "tree" => [
                "title" => "树",
                "type" => "tree",
                "nodes" => [
                    ["id" => 1, "pid" => 0, "name" => "节点一"],
                    ["id" => 2, "pid" => 0, "name" => "节点二"],
                    ["id" => 3, "pid" => 1, "name" => "节点三"],
                    ["id" => 4, "pid" => 3, "name" => "节点四"],
                    ["id" => 5, "pid" => 2, "name" => "节点五"],
                ]
            ],
        ];
        foreach ($tabField as $k => $v) {
            switch ($tab) {
                case "require":
                    $v["verify"] = "require";
                    $this->setFieldDefault($v);
                    break;
                case "default":
                    $this->setFieldDefault($v);
                    break;
                case "disabled":
                    $v["readonly"] = true;
                    $v["disabled"] = true;
                    break;
            }
            $field["{$tab}_{$k}"] = $v;
        }
        return $field;
    }

    private function setFieldDefault(array &$field) {
        $type = isset($field["type"]) ? $field["type"] : "text";
        if(in_array($type,["select", "radio", "checkbox"])){
            $default = "option1";
        }elseif (in_array($type,["img", "imgs", "file","files","dropdownSearch"])){
            $default = "";
        }else{
            $default = "初始默认值";
        }
        $field["default"] = $default;
    }


    public function dropdownSearchOptions(array $param = []): array {
        $param = $param ?: input('get.');
        $ids = filter_sql($param['ids']);
        $keywords = filter_sql($param['keywords']);

        $where = [];
        if ($ids) $where[] = ['id', 'in', $ids];
        if ($keywords) $where[] = ['name', 'like', "%{$keywords}%"];
        $field = ['id', 'name'];
        $datas = $this->getUserModel()->getOwnRows($where, $field, ["id" => "desc"], 0, 20);
        $items = array_combine(array_column($datas, "id"), array_column($datas, "name"));
        return $items;
    }

}