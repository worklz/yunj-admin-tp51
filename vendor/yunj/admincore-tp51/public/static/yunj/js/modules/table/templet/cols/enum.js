/**
 * TableColEnum
 */
layui.define(['TableColTemplet','jquery','yunj'], function (exports) {

    let TableColTemplet = layui.TableColTemplet;
    let win = window;
    let doc = document;
    let $ = layui.jquery;

    class TableColEnum extends TableColTemplet{

        constructor(options) {
            super(options);
        }

        layout(){
            let that=this;
            return `{{# 
                        let options = JSON.parse('${JSON.stringify(that.args.options)}');
                        let vals = d.${that.key};
                        if(yunj.isString(vals)&&vals.length>0) 
                            vals = yunj.isJson(vals)?JSON.parse(vals):(vals.indexOf(",")!==-1?vals.split(","):[vals]);
                        else if(yunj.isNumber(vals)) 
                            vals = [vals];
                        if(!yunj.isArray(vals)) vals = [];
                        let text = "";
                        for(let i=0,l=vals.length;i<l;i++){
                            let val = vals[i];
                            if(options.hasOwnProperty(val)) text+='、'+options[val];
                        }
                        text = text?text.substr(1):text;
                     }}
                     <div class="table-row-enum" title="点击复制">{{ text }}</div>`;
        }

        defineExtraEventBind(){
            let that=this;

            // 防止重复绑定事件
            if (yunj.isUndefined(win.TABLE_ROW_ENUM_CLICK_COPY_EVENT_BIND)) {
                win.TABLE_ROW_ENUM_CLICK_COPY_EVENT_BIND = true;
                $(doc).on('click','.table-row-enum',function (e) {
                    yunj.copy($(this).html());
                    e.stopPropagation();
                });
            }
        }

    }

    exports('TableColEnum', TableColEnum);
});