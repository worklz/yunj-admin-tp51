<?php
namespace yunj\control\field;

class Text extends YunjField {

    private static $instance;

    public static function instance(){
        if (!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs():array{
        return [
            'placeholder' => '',     // 占位符
            'readonly' => false,     // 只读
        ];
    }

}