<?php
namespace yunj\control\field;

class Tree extends YunjField {

    private static $instance;

    public static function instance(){
        if (!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs():array{
        return [
            'mode' => 'checkbox',               // 模式（可选值：radio、checkbox默认）
            'nodes' => [],                      // 节点（示例:[["id":1,"name":"xiaowang","pid":0],["id":2,"name":"xiaoli","pid":1]]）
            'allOptional' => false,            // 所有可选（默认false），反之末级可选
            'retractLevel' => -1,               // 收起等级（默认-1不收起），从0开始取值
            'disabled'=>false                  // 禁用
        ];
    }

    protected function handleArgs($args): array {
        switch ($args["mode"]){
            case "checkbox":
                if (!strstr($args["verify"], "arrayIn"))
                    $args["verify"] .= ($args["verify"] ? "|" : "") . "arrayIn:" . implode(",", array_column($args["nodes"],"id"));
                break;
            case "radio":
                if (!strstr($args["verify"], "in"))
                    $args["verify"] .= ($args["verify"] ? "|" : "") . "in:" . implode(",", array_column($args["nodes"],"id"));
                break;
        }
        return $args;
    }

}