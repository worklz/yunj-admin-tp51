/**
 * FormFieldFile
 */
layui.define(['FormField', 'yunj', 'upload'], function (exports) {

    let FormField = layui.FormField;
    let $ = layui.jquery;
    let upload = layui.upload;

    class FormFieldFile extends FormField {

        constructor(options = {}) {
            super(options);
            // 上传操作元素elem
            this.upload_action_elem = `#${this.id} .file-upload-box .upload-action-elem`;
            // 预览容器dom元素对象
            this.file_preview_box_el = null;
            // 上传文件
            this.upload_files = {};
            // 上传进度定时器
            this.upload_progress_timer = null;
            // 上传进度
            this.upload_progress_rate_num = 0;
        }

        defineExtraArgs() {
            let that = this;
            return {
                size: yunj.config("file.upload_file_size"),
                ext: yunj.config("file.upload_file_ext"),
                text: "文件上传",
                disabled: false
            };
        }

        handleArgs(args) {
            if (args.verify.indexOf("mapHas") === -1)
                args.verify += (args.verify ? "|" : "") + "mapHas:name,url";
            return args;
        }

        layoutControl() {
            let that = this;
            let controlHtml = `<div class="file-upload-box">
                                    <div class="file-select-box" title="${that.args.disabled ? '禁用' : '选择文件上传'}">
                                        <i class="layui-icon layui-icon-upload"></i>
                                        <span>${that.args.text}</span>
                                    </div>
                                    <i class="upload-action-elem"></i>
                                </div>`;
            return `<div class="layui-input-inline yunj-form-item-control">${controlHtml}</div>`;
        }

        // 设置字段简介
        layoutDesc() {
            let that = this;
            return `<div class="yunj-form-item-desc">允许上传大小不超过 ${that.args.size}MB 且格式为 ${that.args.ext} 的文件。${that.args.desc || ""}</div>`;
        }

        setValue(val = '') {
            let that = this;
            that.setFilePreviewBoxLayout(val);
        }

        getValue() {
            let that = this;

            if (that.fieldBoxEl.find('.file-preview-box .file-preview-input').length <= 0
                || that.fieldBoxEl.find('.file-preview-box .file-preview-action-box .file-download-btn').length <= 0) return "";
            let name = that.fieldBoxEl.find('.file-preview-box .file-preview-input').val();
            if (!name || !yunj.isString(name) || name.length > 2000) return "";
            let url = that.fieldBoxEl.find('.file-preview-box .file-preview-action-box .file-download-btn').data('url');
            if (!url || !yunj.isString(url) || url.length > 2000) return "";

            return {name: yunj.fileNameExt(name, url), url: url};
        }

        setFilePreviewBoxLayout(file = '') {
            let that = this;
            if (yunj.isString(file)) file = yunj.isJson(file) ? JSON.parse(file) : {};
            if (!yunj.isObj(file) || yunj.isEmptyObj(file)) {
                if (that.file_preview_box_el) that.file_preview_box_el.remove();
                that.file_preview_box_el = null;
                return false;
            }

            if (!that.file_preview_box_el) {
                let previewHtml = `<div class="file-preview-box">
                                        <input type="text" class="layui-input file-preview-input" placeholder="文件名称" ${that.args.disabled ? 'readonly' : ''}>
                                    </div>`;
                that.fieldBoxEl.find('.file-upload-box').prepend(previewHtml);
                that.file_preview_box_el = that.fieldBoxEl.find('.file-upload-box .file-preview-box');
            }
            that.file_preview_box_el.find('.file-preview-input').val(file.name);
            that.set_file_preview_action_box_layout({file_name: file.name, file_url: file.url});
        }

        set_file_preview_action_box_layout(args = {}) {
            args = yunj.objSupp(args, {
                progress: -1,
                file_name: '',
                file_url: '',
            });
            let that = this;
            let filePreviewActionBoxEl = that.file_preview_box_el.find('.file-preview-action-box');
            if (filePreviewActionBoxEl.length <= 0) {
                that.file_preview_box_el.append(`<span class="file-preview-action-box"></span>`);
                filePreviewActionBoxEl = that.file_preview_box_el.find('.file-preview-action-box');
            }
            let boxHtml = args.progress >= 0
                ? `<i class="file-preview-action file-upload-progress">${args.progress}%</i>`
                : `<i class="layui-icon layui-icon-download-circle file-preview-action file-download-btn" title="下载${args.file_name ? (':' + args.file_name) : ''}" data-url="${args.file_url}"></i>
                  <i class="layui-icon layui-icon-delete file-preview-action file-delete-btn" title="${that.args.disabled ? '禁用' : '删除'}"></i>`;
            filePreviewActionBoxEl.html(boxHtml);
        }

        // 设置上传进度数据
        set_upload_progress_data(args = {}) {
            let defArgs = {
                rate_num: 0
            };
            args = yunj.objSupp(args, defArgs);
            let that = this;
            that.upload_progress_rate_num = args.rate_num;

            that.set_file_preview_action_box_layout({progress: args.rate_num, is_upload: true})
        }

        // 上传开始
        upload_progress_start() {
            let that = this;

            that.set_upload_progress_data({
                rate_num: 0,
            });
            that.upload_progress_timer = setInterval(function () {
                let rateNum = that.upload_progress_rate_num;
                rateNum = rateNum + Math.random() * 10 | 0;
                rateNum = rateNum > 99 ? 99 : rateNum;

                that.set_upload_progress_data({
                    rate_num: rateNum,
                });
            }, 300 + Math.random() * 1000);
        }

        // 上传结束
        upload_progress_end(data) {
            let that = this;

            clearInterval(that.upload_progress_timer);

            that.set_upload_progress_data({
                rate_num: 100,
            });
            setTimeout(function () {
                that.setFilePreviewBoxLayout({
                    name: data.fileName,
                    url: data.url,
                });
            }, 1000);
        }

        // 上传错误
        upload_error(tips = '') {
            let that = this;
            clearInterval(that.upload_progress_timer);
            yunj.alert(tips);
            that.setFilePreviewBoxLayout();
        }

        // 设置上传事件绑定
        set_upload_event_bind() {
            let that = this;

            let args = {
                elem: `#${that.id} .file-select-box`,
                url: yunj.fileUploadUrl(),
                accept: 'file',
                exts: that.args.ext.replace(/,/g, '|'),
                auto: false,
                bindAction: that.upload_action_elem,
                field: 'file',
                size: that.args.size * 1024,
                drag: false,
                choose: function (obj) {
                    if (!yunj.isObj(that.upload_files) || !yunj.isEmptyObj(that.upload_files)) {
                        yunj.msg('待上传完成后，执行此操作');
                        return false;
                    }
                    that.upload_files = obj.pushFile();
                    let file = {};
                    let i = 0;
                    for (let index in that.upload_files) {
                        if (!that.upload_files.hasOwnProperty(index)) continue;
                        if (i > 0) delete that.upload_files[index];
                        let uploadFile = that.upload_files[index];
                        file = {name: uploadFile.name, url: ''}
                    }
                    that.setFilePreviewBoxLayout(file);
                    that.upload_progress_start();

                    $(that.upload_action_elem).click();
                },
                done: function (res, index, upload) {
                    delete that.upload_files[index];
                    if (res.errcode !== 0) {
                        that.upload_error(res.msg);
                        return false;
                    }
                    that.upload_progress_end(res.data);
                }
            };

            if (!yunj.isMobile()) args.acceptMime = that.args.ext.split(',').map(item => {
                return `.${item}`;
            }).join(',');

            upload.render(args);
        }

        defineExtraEventBind() {
            let that = this;

            if (!that.args.disabled) {
                // 上传
                that.set_upload_event_bind();

                // 删除
                that.fieldBoxEl.on('click', '.file-preview-box .file-preview-action-box .file-delete-btn', function (e) {
                    that.setValue();
                });
            }

            // 下载
            that.fieldBoxEl.on('click', '.file-preview-box .file-preview-action-box .file-download-btn', function (e) {
                let file = that.getValue();
                if (yunj.isEmptyObj(file)) return false;
                yunj.download({name: file.name, url: file.url});
            });
        }

    }

    exports('FormFieldFile', FormFieldFile);
});