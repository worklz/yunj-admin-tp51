<?php
namespace yunj\control\field;

class DropdownSearch extends YunjField {

    private static $instance;

    public static function instance(){
        if (!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs():array{
        return [
            'url' => '',             // 请求地址
            'multi' => true,        // 模式（是否多选，默认多选）
            'disabled'=>false       // 禁用
        ];
    }

}