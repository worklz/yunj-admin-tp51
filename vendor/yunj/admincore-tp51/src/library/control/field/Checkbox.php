<?php

namespace yunj\control\field;

class Checkbox extends YunjField {

    private static $instance;

    public static function instance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs(): array {
        return [
            'options' => [],         // 选项（例：['read'=>'阅读','write'=>'写作']）
            'disabled' => false       // 禁用
        ];
    }

    protected function handleArgs($args): array {
        if (!strstr($args["verify"], "arrayIn"))
            $args["verify"] .= ($args["verify"] ? "|" : "") . "arrayIn:" . implode(",", array_keys($args["options"]));
        return $args;
    }

}