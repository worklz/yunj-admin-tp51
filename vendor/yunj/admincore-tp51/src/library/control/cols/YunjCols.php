<?php

namespace yunj\control\cols;

class YunjCols {

    protected $requireArgs = [
        'field' => '',              // 设定字段名
        'title' => '',              // 标题（可选）
        'type' => 'normal',         // 列类型（可选，可选值：normal,checkbox）
        'align' => 'left',          // 排列方式（可选，可选值：left,center,right）
        'sort' => false,           // 是否允许排序（可选，默认：false）
        'fixed' => false,          // 固定列（可选，可选值：left,right）
        'hide' => 'no',             // 初始隐藏列（可选，可选值：yes,no,mobile_hide）
        'templet' => '',            // 自定义列模板（可选，可选值：code）
    ];

    // 表头key
    protected $key;

    // 表头配置
    protected $args;

    /**
     * Notes: 设置属性
     * Author: Uncle-L
     * Date: 2020/9/24
     * Time: 14:56
     * @param $key
     * @param $args
     * @return $this
     */
    public function setAttr($key, $args) {
        $this->key = $key;
        $this->setArgs($args);
        return $this;
    }

    /**
     * Notes: 设置字段配置
     * Author: Uncle-L
     * Date: 2020/8/10
     * Time: 12:14
     * @param $args
     */
    final protected function setArgs($args) {
        $args = array_supp($args, $this->defineExtraArgs() + $this->requireArgs);
        $args = $this->handleArgs($args);
        $args['field'] = $this->key;
        $templet = $args['templet'];
        if ($templet && !is_string($templet) && is_callable($templet)) $args['templet'] = call_user_func($templet);
        $this->args = $args;
    }

    /**
     * Notes: 定义额外参数
     * Author: Uncle-L
     * Date: 2020/12/3
     * Time: 10:19
     * @return array
     */
    protected function defineExtraArgs(): array {
        return [];
    }

    /**
     * Notes: 处理 args
     * Author: Uncle-L
     * Date: 2020/12/3
     * Time: 10:22
     * @param array $args
     * @return array
     */
    protected function handleArgs(array $args): array {
        return $args;
    }

    /**
     * Notes: 返回字段配置
     * Author: Uncle-L
     * Date: 2020/8/10
     * Time: 12:13
     * @return mixed
     */
    public function args() {
        return $this->args;
    }

}