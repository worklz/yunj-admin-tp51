<?php
namespace yunj;

/**
 * 对外暴露数据
 * Class YunjExpose
 * @package yunj
 */
final class YunjExpose{

    public static function config($is_json=false){
        $data = [
            "admin"=>[
                // 后台登录页路由地址
                "login_url"=>yunj_config('admin.login_url'),
            ],
            "file"=>[
                // 上传图片后缀
                'upload_img_ext'=>yunj_config('file.upload_img_ext'),
                // 上传图片大小（单位MB）
                'upload_img_size'=>yunj_config('file.upload_img_size'),
                // 上传文件后缀
                'upload_file_ext'=>yunj_config('file.upload_file_ext'),
                // 上传文件大小（单位MB）
                'upload_file_size'=>yunj_config('file.upload_file_size'),
                // 上传媒体后缀
                'upload_media_ext'=>yunj_config('file.upload_media_ext'),
                // 上传媒体大小（单位MB）
                'upload_media_size'=>yunj_config('file.upload_media_size'),
            ]
        ];
        return $is_json?json_encode($data):$data;
    }

}