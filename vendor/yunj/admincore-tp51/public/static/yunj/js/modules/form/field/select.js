/**
 * FormFieldSelect
 */
layui.define(['FormField','form'], function (exports) {

    let FormField = layui.FormField;
    let form = layui.form;

    class FormFieldSelect extends FormField {

        constructor(options={}) {
            super(options);
        }

        defineExtraArgs(){
            let that = this;
            return {
                options:[],
                search:false,
                disabled:false
            };
        }

        handleArgs(args) {
            let optionKeys = Object.keys(args.options);
            if (args.default === "")
                args.default = optionKeys[0];
            if (args.verify.indexOf("in") === -1)
                args.verify += (args.verify ? "|" : "") + `in:${optionKeys.join(",")}`;
            return args;
        }

        layoutControl() {
            let that = this;
            let optionsHtml = '';
            let options = that.args.options;
            for (let k in options) {
                optionsHtml += `<option value="${k}">${options[k]}</option>`;
            }
            let controlHtml = `<select name="${that.id}" lay-filter="${that.id}" ${that.args.disabled ? 'disabled' : ''} ${that.args.search ? 'lay-search' : ''}>${optionsHtml}</select>`;
            return `<div class="layui-input-inline yunj-form-item-control">${controlHtml}</div>`;
        }

        setValue(val=''){
            let that=this;
            that.fieldBoxEl.find(`select[name=${that.id}] option`).prop('selected', false);
            if(val) that.fieldBoxEl.find(`select[name=${that.id}] option[value=${val}]`).prop('selected', true);
            form.render('select', that.tabFormFilter);
        }

        getValue(){
            let that=this;
            return that.fieldBoxEl.find(`select[name=${that.id}]`).val();
        }

        renderDone(){
            let that = this;
            form.render('select', that.tabFormFilter);
        }

    }

    exports('FormFieldSelect', FormFieldSelect);
});