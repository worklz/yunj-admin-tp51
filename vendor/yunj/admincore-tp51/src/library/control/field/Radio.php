<?php

namespace yunj\control\field;

class Radio extends YunjField {

    private static $instance;

    public static function instance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs(): array {
        return [
            'options' => [],         // 选项（例：['man'=>'男','woman'=>'女']）
            'disabled' => false       // 禁用
        ];
    }

    protected function handleArgs($args): array {
        $optionKeys = array_keys($args["options"]);
        if ($args["default"] === "")
            $args["default"] = $optionKeys[0];
        if (!strstr($args["verify"], "in"))
            $args["verify"] .= ($args["verify"] ? "|" : "") . "in:" . implode(",", $optionKeys);
        return $args;
    }

}