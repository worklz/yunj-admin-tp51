<?php

namespace yunj\control\field;

class YSwitch extends YunjField {

    private static $instance;

    public static function instance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs(): array {
        return [
            'text' => 'ON|OFF',         // 显示文本（可选，定义两种状态的文本，前者表示开，后者代表关，默认ON|OFF）
            'disabled' => false       // 禁用
        ];
    }

    protected function handleArgs($args): array {
        if (!strstr($args["verify"], "boolean") && !strstr($args["verify"], "bool"))
            $args["verify"] .= ($args["verify"] ? "|" : "") . "boolean";
        return $args;
    }

}