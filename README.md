# 云静Admin TP5.1

[![](https://img.shields.io/badge/Author-Uncle.L-orange.svg)](https://gitee.com/worklz/yunj-admin-tp51)
[![](https://img.shields.io/badge/version-v1.06.701-brightgreen.svg)](https://gitee.com/worklz/yunj-admin-tp51)
[![star](https://gitee.com/worklz/yunj-admin-tp51/badge/star.svg?theme=dark)](https://gitee.com/worklz/yunj-admin-tp51/stargazers)
[![fork](https://gitee.com/worklz/yunj-admin-tp51/badge/fork.svg?theme=dark)](https://gitee.com/worklz/yunj-admin-tp51)

## 简介

**云静Admin TP5.1**是一款免费开源且自适应移动/PC的PHP后台开发框架，基于ThinkPHP5.1 + Layui集成了表单/表格构建器等相关功能模块，以方便开发者快速构建自己的应用。在使用过程中，若发现待优化问题请及时与我们取得联系。
<p align="right" >—— Uncle.L</p>

## 安装

注意：最近phpcomposer镜像存在问题，可以改成：`composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/`

```
composer create-project yunj/admin-tp51 blog
```

版本更新
```
composer update yunj/admincore-tp51
```

## 演示截图

PC端截图
![](https://img.kancloud.cn/35/92/3592ab383875e61ed03df803a5d8bdb8_1920x902.png)
移动端截图
![](https://img.kancloud.cn/f7/af/f7af24b631219679d6474bd373ac95f8_1920x811.png)

## 演示地址

* 地址一：[http://tp51admin.iyunj.cn/admin](http://tp51admin.iyunj.cn/admin)
* 地址二：[http://tp51admin.lzadmin.top/admin](http://tp51admin.lzadmin.top/admin)

## 文档

[https://www.kancloud.cn/worklz/yunj_admin_tp51/2526381](https://www.kancloud.cn/worklz/yunj_admin_tp51/2526381)

## QQ交流群

* `1154038256`

## 参与开发

直接提交PR或者Issue即可