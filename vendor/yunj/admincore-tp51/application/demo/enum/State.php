<?php

namespace app\demo\enum;

final class State extends Enum {

    const NORMAL = 11;

    const RECYLE_BIN = 22;

    const DELETED = 33;

    public static function getTitleMap(): array {
        return [
            self::NORMAL => "正常",
            self::RECYLE_BIN => "回收站",
            self::DELETED => "已删除",
        ];
    }

    public static function getTableState() {
        $options = self::getTitleMap();
        unset($options[self::DELETED]);
        return $options;
    }

}