<?php
// +----------------------------------------------------------------------
// | 云静Admin
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2020 http://www.iyunj.cn
// +----------------------------------------------------------------------
// | 云静Admin提供个人非商业用途免费使用。
// +----------------------------------------------------------------------
// | Author: Uncle-L <1732983738@qq.com>
// +----------------------------------------------------------------------
// | 自定义组件配置
// +----------------------------------------------------------------------

return [

    // 表单字段类型
    'fields'=>[
        // 隐藏域
        'hidden'=>[
            'args'=>'\\yunj\\control\\field\\Hidden',
            'module'=>'/static/yunj/js/modules/form/field/hidden.min',
        ],
        // 文本框
        'text'=>[
            'args'=>'\\yunj\\control\\field\\Text',
            'module'=>'/static/yunj/js/modules/form/field/text.min',
        ],
        // 文本域
        'textarea'=>[
            'args'=>'\\yunj\\control\\field\\Textarea',
            'module'=>'/static/yunj/js/modules/form/field/textarea.min',
        ],
        // 密码框
        'password'=>[
            'args'=>'\\yunj\\control\\field\\Password',
            'module'=>'/static/yunj/js/modules/form/field/password.min',
        ],
        // 富文本
        'editor'=>[
            'args'=>'\\yunj\\control\\field\\Editor',
            'module'=>'/static/yunj/js/modules/form/field/editor.min',
        ],
        // 文档编辑
        'markdown'=>[
            'args'=>'\\yunj\\control\\field\\Markdown',
            'module'=>'/static/yunj/js/modules/form/field/markdown.min',
        ],
        // 下拉选框
        'select'=>[
            'args'=>'\\yunj\\control\\field\\Select',
            'module'=>'/static/yunj/js/modules/form/field/select.min',
        ],
        // 单选框
        'radio'=>[
            'args'=>'\\yunj\\control\\field\\Radio',
            'module'=>'/static/yunj/js/modules/form/field/radio.min',
        ],
        // 复选框
        'checkbox'=>[
            'args'=>'\\yunj\\control\\field\\Checkbox',
            'module'=>'/static/yunj/js/modules/form/field/checkbox.min',
        ],
        // 开关
        'switch'=>[
            'args'=>'\\yunj\\control\\field\\YSwitch',
            'module'=>'/static/yunj/js/modules/form/field/switch.min',
        ],
        // 日期
        'date'=>[
            'args'=>'\\yunj\\control\\field\\Date',
            'module'=>'/static/yunj/js/modules/form/field/date.min',
        ],
        // 时间日期
        'datetime'=>[
            'args'=>'\\yunj\\control\\field\\Datetime',
            'module'=>'/static/yunj/js/modules/form/field/datetime.min',
        ],
        // 年
        'year'=>[
            'args'=>'\\yunj\\control\\field\\Year',
            'module'=>'/static/yunj/js/modules/form/field/year.min',
        ],
        // 月
        'month'=>[
            'args'=>'\\yunj\\control\\field\\Month',
            'module'=>'/static/yunj/js/modules/form/field/month.min',
        ],
        // 时间
        'time'=>[
            'args'=>'\\yunj\\control\\field\\Time',
            'module'=>'/static/yunj/js/modules/form/field/time.min',
        ],
        // 单图
        'img'=>[
            'args'=>'\\yunj\\control\\field\\Img',
            'module'=>'/static/yunj/js/modules/form/field/img.min',
        ],
        // 多图
        'imgs'=>[
            'args'=>'\\yunj\\control\\field\\Imgs',
            'module'=>'/static/yunj/js/modules/form/field/imgs.min',
        ],
        // 单文件
        'file'=>[
            'args'=>'\\yunj\\control\\field\\File',
            'module'=>'/static/yunj/js/modules/form/field/file.min',
        ],
        // 多文件
        'files'=>[
            'args'=>'\\yunj\\control\\field\\Files',
            'module'=>'/static/yunj/js/modules/form/field/files.min',
        ],
        // 取色器
        'color'=>[
            'args'=>'\\yunj\\control\\field\\Color',
            'module'=>'/static/yunj/js/modules/form/field/color.min',
        ],
        // 地区联动
        'area'=>[
            'args'=>'\\yunj\\control\\field\\Area',
            'module'=>'/static/yunj/js/modules/form/field/area.min',
        ],
        // 下拉搜索
        'dropdownSearch'=>[
            'args'=>'\\yunj\\control\\field\\DropdownSearch',
            'module'=>'/static/yunj/js/modules/form/field/dropdown-search.min',
        ],
        // 树形
        'tree'=>[
            'args'=>'\\yunj\\control\\field\\Tree',
            'module'=>'/static/yunj/js/modules/form/field/tree.min',
        ],
    ],

    // 表格表头模板
    'cols'=>[
        // 操作栏
        'action'=>[
            'args'=>'\\yunj\\control\\cols\\templet\\Action',
            'module'=>'/static/yunj/js/modules/table/templet/cols/action.min',
        ],
        // 拖拽排序
        'dragSort'=>[
            'args'=>'\\yunj\\control\\cols\\templet\\DragSort',
            'module'=>'/static/yunj/js/modules/table/templet/cols/drag-sort.min',
        ],
        // 单图预览
        'datetime'=>[
            'args'=>'\\yunj\\control\\cols\\templet\\Datetime',
            'module'=>'/static/yunj/js/modules/table/templet/cols/datetime.min',
        ],
        // 单图预览
        'img'=>[
            'args'=>'\\yunj\\control\\cols\\templet\\Img',
            'module'=>'/static/yunj/js/modules/table/templet/cols/img.min',
        ],
        // 多预览
        'imgs'=>[
            'args'=>'\\yunj\\control\\cols\\templet\\Imgs',
            'module'=>'/static/yunj/js/modules/table/templet/cols/imgs.min',
        ],
        // 单文件预览
        'file'=>[
            'args'=>'\\yunj\\control\\cols\\templet\\File',
            'module'=>'/static/yunj/js/modules/table/templet/cols/file.min',
        ],
        // 多文件预览
        'files'=>[
            'args'=>'\\yunj\\control\\cols\\templet\\Files',
            'module'=>'/static/yunj/js/modules/table/templet/cols/files.min',
        ],
        // 颜色呈现
        'color'=>[
            'args'=>'\\yunj\\control\\cols\\templet\\Color',
            'module'=>'/static/yunj/js/modules/table/templet/cols/color.min',
        ],
        // 地区呈现
        'area'=>[
            'args'=>'\\yunj\\control\\cols\\templet\\Area',
            'module'=>'/static/yunj/js/modules/table/templet/cols/area.min',
        ],
        // 枚举
        'enum'=>[
            'args'=>'\\yunj\\control\\cols\\templet\\Enum',
            'module'=>'/static/yunj/js/modules/table/templet/cols/enum.min',
        ],
    ],

];