<?php

namespace yunj\control\cols\type;

use yunj\control\cols\YunjCols;

class CheckBox extends YunjCols {

    private static $instance;

    public static function instance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs(): array {
        return [
            'align' => 'center',
            'fixed' => 'left',
        ];
    }

}