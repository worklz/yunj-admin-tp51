<?php
namespace yunj\control\cols\templet;

use yunj\control\cols\YunjCols;

class Imgs extends YunjCols {

    private static $instance;

    public static function instance(){
        if (!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs():array{
        return [
            'default' => yunj_config("admin.default_img"),  // 默认图片
        ];
    }

}