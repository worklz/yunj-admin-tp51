/**
 * TableColFiles
 */
layui.define(['TableColTemplet','jquery','yunj'], function (exports) {

    let TableColTemplet = layui.TableColTemplet;
    let win = window;
    let doc = document;
    let $ = layui.jquery;

    class TableColFiles extends TableColTemplet{

        constructor(options) {
            super(options);
        }

        layout(){
            let that=this;
            return `{{# 
                         let files = d.${that.key};
                         if(yunj.isString(files)&&files.length>0) 
                            files = yunj.isJson(files) ? JSON.parse(files) : [];
                         if(!yunj.isArray(files)) files = [];
                     }}
                     {{#  if(files.length>0){ }}
                         <div class="table-row-files">
                            {{#  for(let i=0,l=files.length;i<l;i++){  }}
                                {{#  let file=files[i];  }}
                                {{#  let text=file.name+(d.is_export?"（"+file.url+"）":"");  }}
                                {{ i>0?'、':'' }}<a href="javascript:void(0);" title="点击下载" data-name="{{ file.name }}" data-url="{{ file.url }}">{{ text }}</a>
                            {{#  }  }}
                         </div>
                     {{#  } }}`;
        }

        defineExtraEventBind(){
            let that=this;

            // 防止重复绑定事件
            if (yunj.isUndefined(win.TABLE_ROW_FILES_A_CLICK_COPY_EVENT_BIND)) {
                win.TABLE_ROW_FILES_A_CLICK_COPY_EVENT_BIND = true;
                $(doc).on('click','.table-row-files a',function (e) {
                    let aEl=$(this);
                    let name=aEl.data('name');
                    if(!name||name.length<=0) return false;
                    let url=aEl.data('url');
                    if(!url||url.length<=0) return false;
                    name=yunj.fileNameExt(name,url);
                    yunj.confirm(`确认下载文件[${name}]？`,()=>{
                        yunj.download({name:name,url:url});
                    });
                    e.stopPropagation();
                });
            }
        }

    }

    exports('TableColFiles', TableColFiles);
});