<?php
namespace yunj\control\toolbar;

class Dropdown extends YunjToolbar {

    private static $instance;

    public static function instance(){
        if (!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function setArgs($args) {
        $args = array_supp($args, [
                'dropdown'=>true,
            ]+$this->requireArgs);
        $this->args=$args;
    }

}