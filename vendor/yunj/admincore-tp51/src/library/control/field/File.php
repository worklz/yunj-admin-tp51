<?php
namespace yunj\control\field;

class File extends YunjField {

    private static $instance;

    public static function instance(){
        if (!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs():array{
        return [
            'size'=>yunj_config('file.upload_file_size'),   // 限制上传文件大小，默认配置值
            'ext'=>yunj_config('file.upload_file_ext'),     // 限制上传文件后缀，默认配置值
            'text'=>"文件上传",                             // 显示文字
            'disabled'=>false,                             // 禁用
        ];
    }

    protected function handleArgs($args): array {
        if (!strstr($args["verify"], "mapHas"))
            $args["verify"] .= ($args["verify"] ? "|" : "") . "mapHas:name,url";
        return $args;
    }

}