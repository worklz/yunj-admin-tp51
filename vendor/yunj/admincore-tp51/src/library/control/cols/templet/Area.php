<?php
namespace yunj\control\cols\templet;

use yunj\control\cols\YunjCols;

class Area extends YunjCols {

    private static $instance;

    public static function instance(){
        if (!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs():array{
        return [
            'acc'=>'district',     // 精度，可选值：province、city、district
        ];
    }

}