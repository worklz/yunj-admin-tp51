<?php
namespace yunj\control\field;

class Imgs extends YunjField {

    private static $instance;

    public static function instance(){
        if (!self::$instance instanceof self){
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs():array{
        return [
            'max'=>9,                                           // 限制上传数量，默认9
            'size'=>yunj_config('file.upload_img_size'),   // 限制上传图片大小，默认配置值
            'ext'=>yunj_config('file.upload_img_ext'),     // 限制上传图片后缀，默认配置值
            'text'=>"图片上传",                             // 显示文字
            'disabled'=>false,                                 // 禁用
        ];
    }

    protected function handleArgs($args): array {
        if (!strstr($args["verify"], "array"))
            $args["verify"] .= ($args["verify"] ? "|" : "") . "array";
        return $args;
    }

}