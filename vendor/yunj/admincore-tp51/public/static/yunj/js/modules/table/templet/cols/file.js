/**
 * TableColFile
 */
layui.define(['TableColTemplet','jquery','yunj'], function (exports) {

    let TableColTemplet = layui.TableColTemplet;
    let win = window;
    let doc = document;
    let $ = layui.jquery;

    class TableColFile extends TableColTemplet{

        constructor(options) {
            super(options);
        }

        layout(){
            let that=this;
            return `{{# 
                         let file = d.${that.key};
                         if(yunj.isString(file)) file = yunj.isJson(file) ? JSON.parse(file) : {};
                         if(!yunj.isObj(file)) file = {};
                     }}
                     {{#  if(!yunj.isEmptyObj(file)){ }}
                         <div class="table-row-file">
                            {{#  let text=file.name+(d.is_export?"（"+file.url+"）":"");  }}
                            <a href="javascript:void(0);" title="点击下载" data-name="{{ file.name }}" data-url="{{ file.url }}">{{ text }}</a>
                         </div>
                     {{#  } }}`;
        }

        defineExtraEventBind(){
            let that=this;

            // 防止重复绑定事件
            if (yunj.isUndefined(win.TABLE_ROW_FILE_A_CLICK_COPY_EVENT_BIND)) {
                win.TABLE_ROW_FILE_A_CLICK_COPY_EVENT_BIND = true;
                $(doc).on('click','.table-row-file a',function (e) {
                    let aEl=$(this);
                    let name=aEl.data('name');
                    let url=aEl.data('url');
                    yunj.confirm(`确认下载文件[${name}]？`,()=>{
                        if(name.length<=0||url.length<=0) return false;
                        yunj.download({name:name,url:url});
                    });
                    e.stopPropagation();
                });
            }
        }

    }

    exports('TableColFile', TableColFile);
});