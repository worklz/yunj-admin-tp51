<?php

namespace app\demo\controller\controller_demo;

use app\demo\controller\Controller;

class Index extends Controller {

    public function index() {
        return $this->fetch();
    }

}