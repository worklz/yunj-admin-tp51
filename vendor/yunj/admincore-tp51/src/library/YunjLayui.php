<?php

namespace yunj;

use yunj\control\YunjControl;

final class YunjLayui {

    // 系统必须的模块扩展
    private $moduleExtend = [
        'yunj' => '/static/yunj/js/modules/yunj.min',
        'md5' => '/static/yunj/js/modules/md5.min',
        'cookie' => '/static/yunj/js/modules/cookie.min',
        'dropdown' => '/static/yunj/js/modules/dropdown.min',
        'download' => '/static/yunj/js/modules/download.min',
        'button' => '/static/yunj/js/modules/button.min',
        'elemProgress' => '/static/yunj/js/modules/elem-progress.min',
        'loadProgress' => '/static/yunj/js/modules/load-progress.min',
        'tableExport' => '/static/yunj/js/modules/table/export.min',
        'validate' => '/static/yunj/js/modules/validate.min',
        'validateMethods' => '/static/yunj/js/modules/validate-methods.min',
        'FormField' => '/static/yunj/js/modules/form/field/field.min',
        'FormBuild' => '/static/yunj/js/modules/form/build/build.min',
        'FormBuildTab' => '/static/yunj/js/modules/form/build/tab.min',
        'FormBuildField' => '/static/yunj/js/modules/form/build/field.min',
        'FormBuildButton' => '/static/yunj/js/modules/form/build/button.min',
        'TableBuild' => '/static/yunj/js/modules/table/build/build.min',
        'TableBuildState' => '/static/yunj/js/modules/table/build/state.min',
        'TableBuildFilter' => '/static/yunj/js/modules/table/build/filter.min',
        'TableBuildLayTable' => '/static/yunj/js/modules/table/build/lay-table.min',
        'ImportStep' => '/static/yunj/js/modules/import/step/step.min',
        'ImportStepOne' => '/static/yunj/js/modules/import/step/one.min',
        'ImportStepTwo' => '/static/yunj/js/modules/import/step/two.min',
        'ImportStepThree' => '/static/yunj/js/modules/import/step/three.min',
        'TableTemplet' => '/static/yunj/js/modules/table/templet/templet.min',
        'tableToolbar' => '/static/yunj/js/modules/table/templet/toolbar.min',
        'TableColTemplet' => '/static/yunj/js/modules/table/templet/cols/templet.min',
        'TableColCustom' => '/static/yunj/js/modules/table/templet/cols/custom.min',
    ];

    private static $instance;

    public static function instance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function moduleExtend() {
        static $datas;
        if ($datas) return $datas;
        $datas = $this->moduleExtend + YunjControl::instance()->fieldModuleArr + YunjControl::instance()->colsModuleArr;
        $datas = json_encode($datas);
        return $datas;
    }

}