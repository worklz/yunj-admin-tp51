<?php

namespace yunj\control\cols\templet;

use yunj\control\cols\YunjCols;

class Action extends YunjCols {

    // 操作栏操作项必要配置
    private $actionRequireArgs = [
        'title' => '',                // 标题
        'type' => '',               // 触发事件类型（openTab 打开子页面、openPopup 打开弹出层页面、asyncEvent 异步事件）
        'class' => '',                // 额外class（可选，可含图标的class）
        'url' => '',                  // 触发事件执行的url
        'confirmText' => '',          // 确认弹窗提示（可选）
        'dropdown' => false,          // 是否下拉操作
    ];

    private static $instance;

    public static function instance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function defineExtraArgs(): array {
        return [
            'title' => '操作',
            'align' => 'center',
            'fixed' => 'right',
            'options' => [],
        ];
    }

    protected function handleArgs($args): array {
        $options = $args['options'];
        if (!$options) return $args;
        $args['options'] = [];
        foreach ($options as $k => $v) {
            $option = array_supp($v, $this->actionRequireArgs);
            $option['event'] = $k;
            $args['options'][] = $option;
        }
        return $args;
    }

}