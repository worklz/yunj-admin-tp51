<?php

namespace yunj\enum;

use yunj\library\enum\Enum as YunjEnum;

abstract class Enum extends YunjEnum {

    public static function getTitleMap(): array {
        return [];
    }

    public function getTitle(){
        return $this->match(static::getTitleMap());
    }

    public static function getValueByTitle(string $title){
        return array_search($title,static::getTitleMap(),true);
    }

}