/**
 * FormBuildButton
 */
layui.define(['jquery', 'yunj', "FormBuild", "button", "validate"], function (exports) {

    let win = window;
    let doc = document;
    let $ = layui.jquery;
    let FormBuild = layui.FormBuild;
    let button = layui.button;
    let validate = layui.validate;

    class FormBuildButton extends FormBuild {

        constructor(form) {
            super(form, "button");
            this.rawPageWin = null;                 // 来源页面window对象
            this.rawTable = null;                   // 来源表格
            this._initButton();
        }

        // 初始化build box el
        _initBuildBoxEl() {
            let that = this;
            let headerContentEl = that.formBoxEl.find(".yunj-form-header .content");
            headerContentEl.append(`<div class="btn-box"></div>`);
            that.buildBoxEl = headerContentEl.find(".btn-box");
        }

        // 初始化build其他属性
        _initButton(){
            let that = this;
            that.rawPageWin = yunj.rawPageWin();
            let urlParam = that.form.urlParam;
            if (that.rawPageWin && urlParam.hasOwnProperty('rawTable'))
                that.rawTable = that.rawPageWin.yunj.table[urlParam.rawTable];
        }

        // 渲染
        async render() {
            let that = this;
            let outbtnArr = [];
            let currWidth = $(doc).width();
            if (currWidth < 375) {
                outbtnArr = ['submit'];
            } else if (currWidth >= 375 && currWidth <= 480) {
                outbtnArr = ['back', 'submit', 'reset'];
            }
            let outbtnArrLen = outbtnArr.length;
            let btnHtml = that.buildBoxEl.html();
            if (outbtnArrLen <= 0 && btnHtml.length > 0) return;
            that.buildBoxEl.html('');
            let ddHtml = '';
            let btnArr = that.buildArgs;
            for (let i in btnArr) {
                if (!btnArr.hasOwnProperty(i)) continue;
                let type = btnArr[i];
                if (!button.hasOwnProperty(type)) continue;
                if (outbtnArrLen <= 0 || outbtnArr.indexOf(type) !== -1) {
                    button[type](that.formId).render(`.yunj-form-box[lay-filter=${that.formId}] .yunj-form-header .btn-box`);
                } else {
                    ddHtml += button[type](that.formId).layout(true);
                }
            }
            if (ddHtml.length > 0) {
                await new Promise(resolve => {
                    layui.use("dropdown", () => {
                        that.buildBoxEl.append(layui.dropdown.layout(ddHtml));
                        resolve();
                    });
                });
            }
        }

        // 数据提交
        _submit() {
            let that = this;
            let formData = yunj.formData(that.formId, validate);
            if (yunj.isEmptyObj(formData)) return;
            let requestData = {
                builderId: that.formId,
                builderAsyncType: 'submit',
                data: JSON.stringify(formData),
            };

            yunj.request(that.form.url, requestData, "post").then(res => {
                // 刷新来源表格或页面
                if (yunj.isObj(res.data) && res.data.hasOwnProperty('reload') && res.data.reload) {
                    if (that.rawTable) {
                        that.rawTable.render();
                    } else if (that.rawPageWin) {
                        that.rawPageWin.location.reload(true);
                    }
                }
                // 关闭弹出层
                if (that.form.isPopup) yunj.closeCurr();
            }).catch(e => {
                console.log(e);
                yunj.error("表单提交失败");
            });

        }

        setEventBind() {
            let that = this;

            // 绑定窗口大小发生变化时触发
            $(doc).bind(`yunj_form_${that.formId}_win_resize`, function (e) {
                that.render();
            });

            if (that.buildArgs.indexOf("return")) {
                // 监听return点击
                that.buildBoxEl.on('click', '.yunj-btn-return', function () {
                    yunj.closeCurr();
                });
            }

            if (that.buildArgs.indexOf("reload")) {
                // 监听reload点击
                that.buildBoxEl.on('click', '.yunj-btn-reload', function () {
                    location.reload(true);
                });
            }

            if (that.buildArgs.indexOf("reset")) {
                // 监听reset点击
                that.buildBoxEl.on('click', '.yunj-btn-reset', function () {
                    $(doc).trigger(`yunj_form_${that.formId}_reset`);
                });
            }

            if (that.buildArgs.indexOf("clear")) {
                // 监听clear点击
                that.buildBoxEl.on('click', '.yunj-btn-clear', function () {
                    $(doc).trigger(`yunj_form_${that.id}_clear`);
                });
            }

            if (that.buildArgs.indexOf("submit")) {
                // 监听submit点击
                that.buildBoxEl.on('click', '.yunj-btn-submit', function () {
                    that._submit();
                });
            }

        }

    }

    exports('FormBuildButton', FormBuildButton);
});