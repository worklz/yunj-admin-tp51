<?php

namespace app\demo\enum;

final class Sex extends Enum {

    const MAN = "man";

    const WOMAN = "woman";

    public static function getTitleMap(): array {
        return [
            self::MAN => "男",
            self::WOMAN => "女",
        ];
    }

}