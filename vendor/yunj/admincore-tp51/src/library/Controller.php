<?php

namespace yunj;

use think\facade\View;
use think\facade\Request;
use think\facade\Response;
use think\Controller as ThinkController;

class Controller extends ThinkController {

    protected function initialize() {
        parent::initialize();
        $shareData = [
            "yunjInit" => view_template("yunj_init"),
            "adminPage"=>view_template("admin/page"),
        ];
        View::share($shareData);
    }

    /**
     * Notes: 获取view对象实例
     * Author: Uncle-L
     * Date: 2021/4/15
     * Time: 12:49
     * @return \think\View
     */
    public function getView() {
        return $this->view;
    }

    /**
     * Notes: 加载模板输出（补充异步请求下的返回值）
     * Author: Uncle-L
     * Date: 2020/3/31
     * Time: 17:26
     * @param  string $template 模板文件名
     * @param  array $vars 模板输出变量
     * @param  array $config 模板参数
     * @return mixed|\think\response\Json
     */
    protected function fetch($template = '', $vars = [], $config = []) {
        if (Request::isAjax()) return response_json(10001);
        return Response::create($template, 'view')->assign($vars)->config($config);
    }
}